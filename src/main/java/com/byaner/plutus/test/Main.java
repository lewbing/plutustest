package com.byaner.plutus.test;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.byaner.plutus.common.utils.data.ObjectUtils;
import com.byaner.plutus.test.controller.BonusController;
import com.byaner.plutus.test.rest.RestHandler;
import com.byaner.plutus.test.rest.RestResult;
import com.byaner.plutus.test.rest.api.RestApiType;

public class Main {
    private static Logger mLogger = LoggerFactory.getLogger(Main.class);

    public static void postEncash() {
        RestHandler restHandler = RestHandler.getInstance();
        RestResult result;

        Map<String, String> reqData = new HashMap<>();

        reqData.put("gameName", "K3");
        reqData.put("logicCode", "36310711");
        reqData.put("termCode", "1203042");
        reqData.put("ticketCode", "nmlikwokwivkjoqqmovn");
        reqData.put("winMoney", "2475");

        mLogger.info("postEncash: reqData=" + ObjectUtils.map2Json(reqData));

        result = restHandler.call(RestApiType.PostEncash, ObjectUtils.map2Json(reqData));

        mLogger.info("postEncash: result=" + result);
    }
    
    public static void validate() {
        RestHandler restHandler = RestHandler.getInstance();
        RestResult result;

        Map<String, String> reqData = new HashMap<>();

        reqData.put("gameName", "K3");
        reqData.put("idCard", "");
        reqData.put("passwd", "");
        reqData.put("phoneNo", "");
        reqData.put("ticketCode", "nmlikwokwivkjoqqmovn");

        mLogger.info("validate: reqData=" + ObjectUtils.map2Json(reqData));

        result = restHandler.call(RestApiType.Validate, ObjectUtils.map2Json(reqData));

        mLogger.info("validate: result=" + result);
    }
    
    public static void asyncBuy() {
        RestHandler restHandler = RestHandler.getInstance();
        RestResult result;

        Map<String, String> reqData = new HashMap<>();

        reqData.put("deviceType", "0");
        reqData.put("lebi", "0");
        reqData.put("betNum", "99");
        reqData.put("betType", "");
        reqData.put("channelNo", "");
        reqData.put("channelStatus", "");
        reqData.put("checkCode", "");
        reqData.put("cityCode", "");
        reqData.put("code", "");
        reqData.put("deviceType", "0");
        reqData.put("drawTime", "");
        reqData.put("drawWay", "");
        reqData.put("gameName", "");
        reqData.put("idCard", "");
        reqData.put("lebi", "0");
        reqData.put("logicCode", "");
        reqData.put("mobileNo", "");
        reqData.put("money", "0");
        reqData.put("operNum", "0");
        reqData.put("phoneCardPasswd", "");
        reqData.put("phoneNo", "");
        reqData.put("recordType", "");
        reqData.put("remain", "0");
        reqData.put("runCode", "");
        reqData.put("saleOrder", "");
        reqData.put("saleTime", "");
        reqData.put("sellDateTime", "");
        reqData.put("sellPersonId", "");
        reqData.put("sellTermCode", "");
        reqData.put("sellWay", "");
        reqData.put("sendTime", "");
        reqData.put("ticketCode", "");
        reqData.put("ticketId", "");
        reqData.put("tranNo", "");
        reqData.put("validTermCode", "");
        reqData.put("withdrawFlag", "0");
        reqData.put("zhGameName", "");
        reqData.put("lotteryType", "0");
        reqData.put("mobileNo", "");
        reqData.put("orderId", "");
        reqData.put("orderMoney", "0");
        reqData.put("payType", "0");
        reqData.put("terminalId", "");
       
        mLogger.info("asyncBuy: reqData=" + ObjectUtils.map2Json(reqData));

        result = restHandler.call(RestApiType.AsyncBuy, ObjectUtils.map2Json(reqData));

        mLogger.info("asyncBuy: result=" + result);
    }
    
    
    public static void notifyPayCallback() {
        RestHandler restHandler = RestHandler.getInstance();
        RestResult result;

        Map<String, String> reqData = new HashMap<>();

        reqData.put("", "");

        mLogger.info("notifyPayCallback: reqData=" + ObjectUtils.map2Json(reqData));

        result = restHandler.call(RestApiType.NotifyPayCallback, ObjectUtils.map2Json(reqData));

        mLogger.info("notifyPayCallback: result=" + result);
    }
    
    public static void notifyChangjiePayCallback() {
        RestHandler restHandler = RestHandler.getInstance();
        RestResult result;

        Map<String, String> reqData = new HashMap<>();

        reqData.put("", "");

        mLogger.info("notifyChangjiePayCallback: reqData=" + ObjectUtils.map2Json(reqData));

        result = restHandler.call(RestApiType.NotifyChangjiePayCallback, ObjectUtils.map2Json(reqData));

        mLogger.info("notifyChangjiePayCallback: result=" + result);
    }
    
    public static void notifyPingAnPayCallback() {
        RestHandler restHandler = RestHandler.getInstance();
        RestResult result;

        Map<String, String> reqData = new HashMap<>();

        reqData.put("", "");

        mLogger.info("notifyPingAnPayCallback: reqData=" + ObjectUtils.map2Json(reqData));

        result = restHandler.call(RestApiType.NotifyPingAnPayCallback, ObjectUtils.map2Json(reqData));

        mLogger.info("notifyPingAnPayCallback: result=" + result);
    }
    
   
    
    public static void addUnpaid() {
        RestHandler restHandler = RestHandler.getInstance();
        RestResult result;

        Map<String, String> reqData = new HashMap<>();

        reqData.put("account", "13055756005");
        reqData.put("cardNum", "6217001820019271778");
        reqData.put("cardUserName", "张三丰");
        reqData.put("channelName", "ChangJieWechatPay");
        reqData.put("channelTranNo", "152354648965654564");
        reqData.put("checkType", "");
        reqData.put("deviceType", "");
        reqData.put("groupNo", "");
        reqData.put("id", "");
        reqData.put("money", "10");
        reqData.put("openId", "");
        reqData.put("orderId", "");
        reqData.put("payType", "");
        reqData.put("saleTime", "");
        reqData.put("sendTime", "");
        reqData.put("terminalId", "");
        reqData.put("tranNo", "");

        mLogger.info("addUnpaid: reqData=" + ObjectUtils.map2Json(reqData));

        result = restHandler.call(RestApiType.AddUnpaid, ObjectUtils.map2Json(reqData));

        mLogger.info("addUnpaid: result=" + result);
    }
    
    public static void printLottery2() {
        RestHandler restHandler = RestHandler.getInstance();
        RestResult result;

        Map<String, String> reqData = new HashMap<>();

        reqData.put("orderId", "CP20181212000071");
        reqData.put("terminalId", "999999989");
        reqData.put("ticketId", "85351969898884321023");
        reqData.put("uniteId", "hwnklupotorlqlqihshn");

        mLogger.info("printLottery2: reqData=" + ObjectUtils.map2Json(reqData));

        result = restHandler.call(RestApiType.PrintLottery2, ObjectUtils.map2Json(reqData));

        mLogger.info("printLottery2: result=" + result);
    }  
    
    public static void updateSoftwareVersion() {
        RestHandler restHandler = RestHandler.getInstance();
        RestResult result;

        result = restHandler.call(RestApiType.UpdateSoftwareVersion, "999999996");  

        mLogger.info("updateSoftwareVersion: result=" + result);
    }
    
    public static void main(String[] args) {
    	
    	// postEncash();  //兑奖期号不合法，暂不测该接口

    	// validate(); //解析字符串失败,暂时不测该接口
    	 	
    	// notifyPayCallback(); //得出结果：ecode=null, message=null, data=null
    	
    	// notifyChangjiePayCallback(); //ecode=40001, message=系统繁忙，请稍后重试, data=null
    	
    	// notifyPingAnPayCallback(); //ecode=40001, message=系统繁忙，请稍后重试, data=null
    	
    	// addUnpaid();   //返回错误500，可能数据不对
    	
    	// printLottery2(); //返回错误400
    	
    	// updateTerminalParameter();  //message是succeeded，但data为空
    	
    	// updateSoftwareVersion();  //接口请求方式有问题
    	
    	
    	/* UserController UserController = new UserController();
        UserController.logout();
        
        BonusController BonusController = new BonusController();
        BonusController.getTerminalParameters();*/
        
    	
    }
}

