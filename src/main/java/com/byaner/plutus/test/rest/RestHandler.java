package com.byaner.plutus.test.rest;

import com.alibaba.fastjson.JSON;
import com.byaner.plutus.common.error.Result;
import com.byaner.plutus.common.error.ResultCode;
import com.byaner.plutus.test.Main;
import com.byaner.plutus.test.rest.api.RestApiType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

public class RestHandler {
    private static Logger mLogger = LoggerFactory.getLogger(Main.class);

//    public static final String SERVER_ADDR = "http://api.byaner.com";
      public static final String SERVER_ADDR = "http://w1.byaner.com";
//    public static final String SERVER_ADDR = "http://127.0.0.1";

    public static final int SERVER_PORT = 9090;

    private RestTemplate mRestTemplate;
    private HttpHeaders mRequestHeaders;

    private static RestHandler mInstance;

    private RestHandler() {
        HttpComponentsClientHttpRequestFactory requestFactory = new HttpComponentsClientHttpRequestFactory();
        requestFactory.setConnectionRequestTimeout(60000);
        requestFactory.setConnectTimeout(60000);// 设置超时
        requestFactory.setReadTimeout(60000);

        mRestTemplate = new RestTemplate(requestFactory);
        mRequestHeaders = new HttpHeaders();
        mRequestHeaders.add("Content-Type", "application/json");
        mRequestHeaders.add("Accept", "*/*");
    }

    public synchronized static RestHandler getInstance() {
        if (mInstance == null) {
            mInstance = new RestHandler();
        }

        return mInstance;
    }

    public RestResult call(RestApiType apiType, String ... params) {
        String url = apiType.getUrl(params);
        mLogger.info("Call service [{}], url [{}], params [{}]", apiType.getOperation(), url, params);

        switch (apiType.getOperation()) {
            case Get:
                return get(url);
            case Post:
                return post(url, params[0]);
            case Put:
                return put(url, params[0]);
        }

        return null;
    }

    private RestResult get(String url) {
        mLogger.info("post: url=" + url);

        HttpEntity<String> requestEntity = new HttpEntity<String>("", mRequestHeaders);
        ResponseEntity<String> responseEntity = mRestTemplate.exchange(url, HttpMethod.GET, requestEntity, String.class);

        return parseResponse(responseEntity);
    }

    private RestResult post(String url, String params) {
        mLogger.info("post: url=" + url + ", params=" + params);

        HttpEntity<String> requestEntity = new HttpEntity<String>(params, mRequestHeaders);
        ResponseEntity<String> responseEntity = mRestTemplate.exchange(url, HttpMethod.POST, requestEntity, String.class);

        return parseResponse(responseEntity);
    }

    private RestResult put(String url, String json) {
        HttpEntity<String> requestEntity = null;
        if (json != null) {
            requestEntity = new HttpEntity<String>(json, mRequestHeaders);
        }
        ResponseEntity<String> responseEntity = mRestTemplate.exchange(url, HttpMethod.PUT, requestEntity, String.class);

        return parseResponse(responseEntity);
    }

    private RestResult parseResponse(ResponseEntity<String> responseEntity) {
        RestResult restResult = new RestResult();

        try {
            int code = responseEntity.getStatusCodeValue();
            if (code == 200 || code == 204) {
                restResult.setResponse(responseEntity.getBody());

                return restResult;
            } else {
                throw new Exception("Error calling reset service!");
            }
        } catch (Exception e) {
            mLogger.error("Error during service call", e);

            Result result = new Result();
            result.setResultCode(ResultCode.SYSTEM_INNER_ERROR);

            restResult.setResponse(JSON.toJSONString(result));
        }

        return restResult;
    }
}
