package com.byaner.plutus.test.rest.api;

import com.byaner.plutus.test.rest.RestHandler;

/**
 * Service operations list.
 *
 * TODO: More operations to add.
 *
 * @author Andrew
 *
 */
public enum RestApiType {
    // The interface of test's controller
    GetTestInfo("/test/info", OperationType.Get),
    GetInfo("/test/info", OperationType.Get),
    GetOrder("/test/db/order/%s", OperationType.Get),
    GetTerminals("/test/db/terminals", OperationType.Get),
    GetTerminal("/test/db/terminals/%s", OperationType.Get),
    PostTestLogin("/test/req/login3", OperationType.Post),

    GetCurrentLotteryParameters("/welfarelott/jiangxi/lottserver//param/lottery/lotteries", OperationType.Get),
    GetTerminalInfo("/terminal/%s", OperationType.Get),
    GetNotice("/welfarelott/jiangxi/bonus/notice/%s", OperationType.Get),
    GetBonusData("/welfarelott/jiangxi/bonus/%s", OperationType.Get),
    GetBonusDataByGameName("/welfarelott/jiangxi/bonus/%s", OperationType.Get),
    //Encash("/welfarelott/jiangxi/encashment/encash", OperationType.Post),
    Encash("/welfarelott/jiangxi/encashment/encash/%s/%s", OperationType.Get),
    GetticketCode("/welfarelott/jiangxi/encashment/%s", OperationType.Get),
    QueryStatus("/welfarelott/jiangxi/lottery/query/status/%s", OperationType.Get),
    GetDateTime("/welfarelott/jiangxi/lottserver/datetime", OperationType.Get),
    QueryMoney("/welfarelott/jiangxi/lottserver/money/%s", OperationType.Get),
    GetLotteryParameters("/welfarelott/jiangxi/lottserver/param/lottery/lotteries", OperationType.Get),
    GetLotteryParameter("/welfarelott/jiangxi/lottserver/param/lottery/%s", OperationType.Get),
    ResetPendingTicketsPrintTime("/order/print/pending/reset/terminal/%s", OperationType.Get),
    GetPendingTerminalPrintTickets("/order/print/pending/terminal/%s", OperationType.Get),
    GetTicket("/order/ticket/%s", OperationType.Get),
    BillList("/payment/billlist/%s", OperationType.Get),
    CancelPayment("/payment/cancel/%s", OperationType.Get),
    OrderDetail("/payment/detail/%s", OperationType.Get),
    BuyLottery("/payment/notify/pay/test/%s", OperationType.Get),
    OrderList("/payment/orderlist", OperationType.Get),
    PayList("/payment/paylist", OperationType.Get),
    Refund("/payment/refund/%s/%s", OperationType.Get),
    RefundStatus("/payment/status/refund/%s", OperationType.Get),
    Status("/payment/status/%s", OperationType.Get),
    GetSellingData("/welfarelott/jiangxi/selling/data/%s/%s", OperationType.Get),
    GetLatestValidSoftwareVersion("/terminal/latestValidSoftwareVersion", OperationType.Get),
    NotifyParameterUpdated("/terminal/parameter/notify_updated", OperationType.Get),
    GetTerminalParameters("/terminal/%s/parameters", OperationType.Get),
    GetSoftwareVersion("/terminal/%s/softwareVersion", OperationType.Get),
    Validate("/welfarelott/jiangxi/encashment/validate", OperationType.Post),
    AsyncBuy("/welfarelott/jiangxi/lottery/async_buy", OperationType.Post),
    Buy("/welfarelott/jiangxi/lottery/buy", OperationType.Post),
    Order("/welfarelott/jiangxi/lottery/order", OperationType.Post),
    TestBuy("/welfarelott/jiangxi/lottery/test_buy", OperationType.Post),
    ValidateLottery("/welfarelott/jiangxi/lottery/validate", OperationType.Post),
    Withdraw("/welfarelott/jiangxi/lottery/withdraw", OperationType.Post),
    AddOrder("/order/add", OperationType.Post),
    Charge("/payment/charge", OperationType.Post),
    NotifyPayCallback("/payment/notify/pay/callback", OperationType.Post),
    NotifyChangjiePayCallback("/payment/notify/pay/changjie_callback", OperationType.Post),
    NotifyPingAnPayCallback("/payment/notify/pay/pingan_callback", OperationType.Post),
    Pay("/payment/pay", OperationType.Post),
    AddUnpaid("/payment/record/add", OperationType.Post),
    PrintLottery("/welfarelott/jiangxi/lottery/print", OperationType.Post),
    PrintLottery2("/welfarelott/jiangxi/lottery/print2", OperationType.Post),
    FailPendingTerminalPrintTickets("/order/print/fail/terminal/%s", OperationType.Put),
    UpdateOrderTicketsPrintStatus("/order/print/ticket/%s/status/%s/reason1/%s/reason2/%s", OperationType.Put),
    UpdateComponentStatus("/terminal/%s/component/%s/status/%s", OperationType.Put),
    UpdateConnectionStatus("/terminal/%s/conn/%s", OperationType.Put),
    UpdateTerminalParameter("/terminal/%s/parameter/%s", OperationType.Put),
    UpdateSoftwareVersion("/terminal/%s/softwareVersion", OperationType.Put),
   
    
    Bind("/user/bind", OperationType.Post),
    Login("/user/auth/login", OperationType.Get),
    Logout("/user/auth/logout", OperationType.Get),
    SignIn("/user/auth/sign_in", OperationType.Post),
    PostEncash("/welfarelott/jiangxi/encashment/encash", OperationType.Post);

    private String uri;
    private OperationType operation;

    RestApiType(String uri, OperationType operationType) {
        this.uri = uri;
        this.operation = operationType;
    }

    public OperationType getOperation() {
        return operation;
    }

    public String getUri() {
        return uri;
    }

    public String getUrl(String... params) {
        if (params != null && params.length > 0) {
            return String.format(getUrl(), params);
        } else {
            return getUrl();
        }
    }

    private String getUrl() {
        return RestHandler.SERVER_ADDR + ":" + RestHandler.SERVER_PORT + uri;
    }
}
