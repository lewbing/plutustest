package com.byaner.plutus.test.rest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;

import lombok.Data;

@Data
public class RestResult {
    private static Logger mLogger = LoggerFactory.getLogger(RestResult.class);

    private static final String RETURN_CODE_FIELD = "code";
    private static final String RETURN_MESSAGE_FIELD = "message";
    private static final String RETURN_DATA_FIELD = "data";

    private String response;
    private JSONObject jsonObject;

    public void setResponse(String response) {
        this.response = response;
        this.jsonObject = JSON.parseObject(response);
    }

    public JSONObject getJsonObject() {
		return jsonObject;
	}

	public void setJsonObject(JSONObject jsonObject) {
		this.jsonObject = jsonObject;
	}

	public String getECode() {
        return getJsonObject() != null ? getJsonObject().getString(RETURN_CODE_FIELD) : null;
    }

    public String getMessage() {
        return getJsonObject() != null ? getJsonObject().getString(RETURN_MESSAGE_FIELD) : null;
    }

    public String getData() {
        return getJsonObject() != null ? getJsonObject().getString(RETURN_DATA_FIELD) : null;
    }

    public String toString() {
        return "RestResult(ecode=" + getECode()
            + ", message=" + getMessage()
            + ", data=" + getData()
            + ")";
    }
}
