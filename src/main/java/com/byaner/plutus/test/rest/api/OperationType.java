package com.byaner.plutus.test.rest.api;

public enum OperationType {
    Get, Post, Put, Delete
}
