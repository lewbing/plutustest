package com.byaner.plutus.test.controller;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.byaner.plutus.common.utils.data.ObjectUtils;
import com.byaner.plutus.test.Main;
import com.byaner.plutus.test.rest.RestHandler;
import com.byaner.plutus.test.rest.RestResult;
import com.byaner.plutus.test.rest.api.RestApiType;

public class BonusController {
	private static Logger mLogger = LoggerFactory.getLogger(Main.class);

    public static void getCurrentLotteryParameters() {
        RestHandler restHandler = RestHandler.getInstance();
        RestResult result;

        result = restHandler.call(RestApiType.GetCurrentLotteryParameters);
        mLogger.info("getCurrentLotteryParameters: result=" + result);
    }

    public static void getTerminalInfo() {
        RestHandler restHandler = RestHandler.getInstance();
        RestResult result;

        result = restHandler.call(RestApiType.GetTerminalInfo, "999999997");  //获取终端信息

        mLogger.info("getTerminalInfo: result=" + result);
    }
    
    public static void getNotice() {
        RestHandler restHandler = RestHandler.getInstance();
        RestResult result;

       // result = restHandler.call(RestApiType.GetNotice, "", "B001");
        result = restHandler.call(RestApiType.GetNotice,"B001","2018614");   //获取双色球2018614期的信息

        mLogger.info("getNotice: result=" + result);
    }
    
    public static void getBonusData() {                                        //获取票面密码
        RestHandler restHandler = RestHandler.getInstance();
        RestResult result;

	      // result = restHandler.call(RestApiType.GetBonusData, "", "hnjskopslijuvsnqipvj"));
        result = restHandler.call(RestApiType.GetBonusData, "B001", "2018614");    //获取双色球玩法2018614期的历史开奖记录

        mLogger.info("getBonusData: result=" + result);
    }
    
    public static void getBonusDataByGameName() {
        RestHandler restHandler = RestHandler.getInstance();
        RestResult result;

        result = restHandler.call(RestApiType.GetBonusDataByGameName, "B001");

        mLogger.info("getBonusDataByGameName: result=" + result);
    }
    
    public static void encash() {
        RestHandler restHandler = RestHandler.getInstance();
        RestResult result;

        result = restHandler.call(RestApiType.Encash, "13055756005", "nphmujpsmsslhkkqrkiv");         //数据未找到

        mLogger.info("encash: result=" + result);
    }
    
    public static void getticketCode() {
        RestHandler restHandler = RestHandler.getInstance();
        RestResult result;

        result = restHandler.call(RestApiType.GetticketCode, "nphmujpsmsslhkkqrkiv");

        mLogger.info("getticketCode: result=" + result);
    }

    public static void queryStatus() {
        RestHandler restHandler = RestHandler.getInstance();
        RestResult result;

        //result = restHandler.call(RestApiType.QueryStatus, "", "79272987679681441765");
        result = restHandler.call(RestApiType.QueryStatus, "2001", "79272987679681441765");  //数据未找到
        mLogger.info("queryStatus: result=" + result);
    }

    public static void getDateTime() {
        RestHandler restHandler = RestHandler.getInstance();
        RestResult result;

        result = restHandler.call(RestApiType.GetDateTime);

        mLogger.info("getDateTime: result=" + result);
    }

    public static void queryMoney() {
        RestHandler restHandler = RestHandler.getInstance();
        RestResult result;

        result = restHandler.call(RestApiType.QueryMoney, "36310711");  //根据逻辑机号查询福彩那边的金额

        mLogger.info("queryMoney: result=" + result);
    }

    public static void getLotteryParameters() {
        RestHandler restHandler = RestHandler.getInstance();
        RestResult result;

        result = restHandler.call(RestApiType.GetLotteryParameters);  //获取彩票参数信息

        mLogger.info("getLotteryParameters: result=" + result);
    }

    public static void getLotteryParameter() {
        RestHandler restHandler = RestHandler.getInstance();
        RestResult result;

       // result = restHandler.call(RestApiType.GetLotteryParameter, "", "B001");  //获取双色球的参数信息
        result = restHandler.call(RestApiType.GetLotteryParameter,"B001", "2018614");  //获取双色球第2018614期的参数信息

        mLogger.info("getLotteryParameter: result=" + result);
    }

    public static void resetPendingTicketsPrintTime() {
        RestHandler restHandler = RestHandler.getInstance();
        RestResult result;

        result = restHandler.call(RestApiType.ResetPendingTicketsPrintTime,"999999993");  //重置终端的时间

        mLogger.info("resetPendingTicketsPrintTime: result=" + result);
    }

    public static void getPendingTerminalPrintTickets() {
        RestHandler restHandler = RestHandler.getInstance();
        RestResult result;

        result = restHandler.call(RestApiType.GetPendingTerminalPrintTickets, "999999993");  //重置终端的时间

        mLogger.info("getPendingTerminalPrintTickets: result=" + result);
    }

    public static void getTicket() {
        RestHandler restHandler = RestHandler.getInstance();
        RestResult result;

        result = restHandler.call(RestApiType.GetTicket, "8014546630532424643");  //

        mLogger.info("getTicket: result=" + result);
    }

    public static void billList() {
        RestHandler restHandler = RestHandler.getInstance();
        RestResult result;

        result = restHandler.call(RestApiType.BillList, "20181205");  //查询当天交易金额

        mLogger.info("billList: result=" + result);
    }

    public static void cancelPayment() {
        RestHandler restHandler = RestHandler.getInstance();
        RestResult result;

        result = restHandler.call(RestApiType.CancelPayment, "CP20181206000102");  //取消已付款订单

        mLogger.info("cancelPayment: result=" + result);
    }

    public static void orderDetail() {
        RestHandler restHandler = RestHandler.getInstance();
        RestResult result;

        result = restHandler.call(RestApiType.OrderDetail, "CP20181206000102");  //获取订单详情

        mLogger.info("orderDetail: result=" + result);
    }

    public static void buyLottery() {
        RestHandler restHandler = RestHandler.getInstance();
        RestResult result;

        result = restHandler.call(RestApiType.BuyLottery, "CP20181206000102");  //获取购买彩票是否成功的状态值

        mLogger.info("buyLottery: result=" + result);
    }

    public static void orderList() {
        RestHandler restHandler = RestHandler.getInstance();
        RestResult result;

        result = restHandler.call(RestApiType.OrderList);  //获取订单列表

        mLogger.info("orderList: result=" + result);
    }

    public static void payList() {
        RestHandler restHandler = RestHandler.getInstance();
        RestResult result;

        result = restHandler.call(RestApiType.PayList);  //获取账单列表

        mLogger.info("payList: result=" + result);
    }

    public static void refund() {
        RestHandler restHandler = RestHandler.getInstance();
        RestResult result;

        result = restHandler.call(RestApiType.Refund, "CP20181207000154", "2");   //退款

        mLogger.info("refund: result=" + result);
    }

    public static void refundStatus() {
        RestHandler restHandler = RestHandler.getInstance();
        RestResult result;

        result = restHandler.call(RestApiType.RefundStatus, "CP20181207000154");   //退款状态

        mLogger.info("refundStatus: result=" + result);
    }

    public static void status() {
        RestHandler restHandler = RestHandler.getInstance();
        RestResult result;

        result = restHandler.call(RestApiType.Status, "CP20181207000154");   //状态

        mLogger.info("status: result=" + result);
    }

    public static void getSellingData() {
        RestHandler restHandler = RestHandler.getInstance();
        RestResult result;

        result = restHandler.call(RestApiType.GetSellingData, "B001", "2018069");

        mLogger.info("getSellingData: result=" + result);
    }

    public static void getLatestValidSoftwareVersion() {
        RestHandler restHandler = RestHandler.getInstance();
        RestResult result;

        result = restHandler.call(RestApiType.GetLatestValidSoftwareVersion);

        mLogger.info("getLatestValidSoftwareVersion: result=" + result);
    }

    public static void notifyParameterUpdated() {
        RestHandler restHandler = RestHandler.getInstance();
        RestResult result;

        result = restHandler.call(RestApiType.NotifyParameterUpdated);

        mLogger.info("notifyParameterUpdated: result=" + result);
    }

    public static void getTerminalParameters() {
        RestHandler restHandler = RestHandler.getInstance();
        RestResult result;

        result = restHandler.call(RestApiType.GetTerminalParameters,"999999996");  //获取终端参数

        mLogger.info("getTerminalParameters: result=" + result);
    }

    public static void getSoftwareVersion() {
        RestHandler restHandler = RestHandler.getInstance();
        RestResult result;

        result = restHandler.call(RestApiType.GetSoftwareVersion,"999999997");  //获取终端软件版本

        mLogger.info("getSoftwareVersion: result=" + result);
    }

    public static void charge() {
        RestHandler restHandler = RestHandler.getInstance();
        RestResult result;

        Map<String, String> reqData = new HashMap<>();

        reqData.put("accountNo", "13055756005");
        reqData.put("bankCardNo", "");
        reqData.put("bankName", "");
        reqData.put("channelName", "ChangJieWechatPay");
        reqData.put("chargeEndTime", "2018-12-09 15:41:08");
        reqData.put("chargeId", "");
        reqData.put("chargeTime", "2018-10-09 15:41:08");
        reqData.put("chargeType", "1");
        reqData.put("deviceType", "0");
        reqData.put("memberId", "");
        reqData.put("money", "11");
        reqData.put("state", "100");

        mLogger.info("charge: reqData=" + ObjectUtils.map2Json(reqData));

        result = restHandler.call(RestApiType.Charge, ObjectUtils.map2Json(reqData));

        mLogger.info("charge: result=" + result);
    }
    
    public static void pay() {
        RestHandler restHandler = RestHandler.getInstance();
        RestResult result;

        Map<String, String> reqData = new HashMap<>();

        reqData.put("account", "13055756005");
        reqData.put("channelName", "ChangJieWechatPay");
        reqData.put("checkType", "");
        reqData.put("deviceType", "");
        reqData.put("lotteryType", "");
        reqData.put("money", "10");
        reqData.put("openId", "");
        reqData.put("orderId", "CP20181212000071");
        reqData.put("payType", "02");
        reqData.put("sendTime", "2018-12-12 11:13:23");
        reqData.put("terminalId", "999999989");
        reqData.put("ticketId", "8764");

        mLogger.info("pay: reqData=" + ObjectUtils.map2Json(reqData));

        result = restHandler.call(RestApiType.Pay, ObjectUtils.map2Json(reqData));

        mLogger.info("pay: result=" + result);
    }
    
    public static void printLottery() {
        RestHandler restHandler = RestHandler.getInstance();
        RestResult result;

        Map<String, String> reqData = new HashMap<>();

        reqData.put("orderId", "CP20181212000071");
        reqData.put("terminalId", "999999989");
        reqData.put("ticketId", "85351969898884321023");
        reqData.put("uniteId", "hwnklupotorlqlqihshn");

        mLogger.info("printLottery: reqData=" + ObjectUtils.map2Json(reqData));

        result = restHandler.call(RestApiType.PrintLottery, ObjectUtils.map2Json(reqData));

        mLogger.info("printLottery: result=" + result);
    }
    
    public static void failPendingTerminalPrintTickets() {
        RestHandler restHandler = RestHandler.getInstance();
        RestResult result;

        result = restHandler.call(RestApiType.FailPendingTerminalPrintTickets, "999999997");  

        mLogger.info("failPendingTerminalPrintTickets: result=" + result);
    }
    
    public static void updateOrderTicketsPrintStatus() {
        RestHandler restHandler = RestHandler.getInstance();
        RestResult result;

        result = restHandler.call(RestApiType.UpdateOrderTicketsPrintStatus, "85351969898884321023","06","7","000100");  

        mLogger.info("updateOrderTicketsPrintStatus: result=" + result);
    }
    
    public static void updateComponentStatus() {
        RestHandler restHandler = RestHandler.getInstance();
        RestResult result;

        result = restHandler.call(RestApiType.UpdateComponentStatus, "999999997","117","00");  

        mLogger.info("updateComponentStatus: result=" + result);
    }
    
    public static void updateConnectionStatus() {
        RestHandler restHandler = RestHandler.getInstance();
        RestResult result;

        result = restHandler.call(RestApiType.UpdateConnectionStatus, "999999997","1");  

        mLogger.info("updateConnectionStatus: result=" + result);
    }
    
    public static void updateTerminalParameter() {
        RestHandler restHandler = RestHandler.getInstance();
        RestResult result;

        result = restHandler.call(RestApiType.UpdateTerminalParameter, "999999996","SERVERPORT=21");  

        mLogger.info("updateTerminalParameter: result=" + result);
    }
    
    

}
