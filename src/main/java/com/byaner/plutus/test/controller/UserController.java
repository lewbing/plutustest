package com.byaner.plutus.test.controller;

import com.byaner.plutus.common.utils.data.ObjectUtils;
import com.byaner.plutus.test.Main;
import com.byaner.plutus.test.rest.RestHandler;
import com.byaner.plutus.test.rest.RestResult;
import com.byaner.plutus.test.rest.api.RestApiType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;

public class UserController {
    private static Logger mLogger = LoggerFactory.getLogger(Main.class);

    public static void login() {                       //注册
        RestHandler restHandler = RestHandler.getInstance();
        RestResult result;

        result = restHandler.call(RestApiType.Login);

        mLogger.info("login: result=" + result);
    }

    public static void logout() {                       //退出登录
        RestHandler restHandler = RestHandler.getInstance();
        RestResult result;

        result = restHandler.call(RestApiType.Logout);

        mLogger.info("logout: result=" + result);
    }

    public static void signIn() {
        RestHandler restHandler = RestHandler.getInstance();
        RestResult result;

        Map<String, String> reqData = new HashMap<>();

        reqData.put("id", "");
        reqData.put("mackey", "");
        reqData.put("machineNo", "");
        reqData.put("masterKey", "");
        reqData.put("pinKey", "");
        reqData.put("terminalKey", "");
        reqData.put("timestamp", "0");
        reqData.put("type", "");

        mLogger.info("signIn: reqData=" + ObjectUtils.map2Json(reqData));

        result = restHandler.call(RestApiType.SignIn, ObjectUtils.map2Json(reqData));

        mLogger.info("signIn: result=" + result);
    }
    
    public static void bind() {
        RestHandler restHandler = RestHandler.getInstance();
        RestResult result;

        Map<String, String> reqData = new HashMap<>();

        reqData.put("mobileNo", "13055756005");
        reqData.put("terminalId", "999999989");
        reqData.put("orderId", "CP20181212000071");

        mLogger.info("bind: reqData=" + ObjectUtils.map2Json(reqData));

        result = restHandler.call(RestApiType.Bind, ObjectUtils.map2Json(reqData));

        mLogger.info("bind: result=" + result);
    }
}
