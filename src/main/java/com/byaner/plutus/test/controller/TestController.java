package com.byaner.plutus.test.controller;

import com.byaner.plutus.common.utils.data.ObjectUtils;
import com.byaner.plutus.test.Main;
import com.byaner.plutus.test.rest.RestHandler;
import com.byaner.plutus.test.rest.RestResult;
import com.byaner.plutus.test.rest.api.RestApiType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;

public class TestController {
    private static Logger mLogger = LoggerFactory.getLogger(Main.class);

    public static void getTestInfo() {
        RestHandler restHandler = RestHandler.getInstance();
        RestResult result;

        result = restHandler.call(RestApiType.GetTestInfo);

        mLogger.info("info: result=" + result);
    }

    public static void getInfo() {
        RestHandler restHandler = RestHandler.getInstance();
        RestResult result;

        result = restHandler.call(RestApiType.GetInfo);

        mLogger.info("getInfo: result=" + result);
    }

    public static void getOrder() {
        RestHandler restHandler = RestHandler.getInstance();
        RestResult result;

        result = restHandler.call(RestApiType.GetOrder, "CP20181205000107");

        mLogger.info("getOrder: result=" + result);
    }

    public static void getTerminals() {                       //获取所有终端信息
        RestHandler restHandler = RestHandler.getInstance();
        RestResult result;

        result = restHandler.call(RestApiType.GetTerminals);

        mLogger.info("getTerminals: result=" + result);
    }

    public static void getTerminal() {                       //根据终端编号获取终端信息
        RestHandler restHandler = RestHandler.getInstance();
        RestResult result;

        result = restHandler.call(RestApiType.GetTerminal, "999999997");

        mLogger.info("getTerminal: result=" + result);
    }

    public static void postTestLogin() {
        RestHandler restHandler = RestHandler.getInstance();
        RestResult result;

        Map<String, String> reqData = new HashMap<>();

        reqData.put("name", "bing");
        reqData.put("passwd", "bing");

        mLogger.info("postTestLogin: reqData=" + ObjectUtils.map2Json(reqData));

        result = restHandler.call(RestApiType.PostTestLogin, ObjectUtils.map2Json(reqData));

        mLogger.info("postTestLogin: result=" + result);
    }
}
