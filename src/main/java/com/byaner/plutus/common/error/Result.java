package com.byaner.plutus.common.error;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by bing on 2018/5/30.
 */
public class Result implements Serializable {
	private static final long serialVersionUID = -3948389268046368059L;

	private int mECode;

	private String mMessage;

	private Object mData;

	public Result() {
	}

	public Result(ResultCode result) {
		setResultCode(result);
	}

	public Result(int ecode, String msg) {
		this.mECode = ecode;
		this.mMessage = msg;
	}

	public void setResultCode(ResultCode result) {
		this.mECode = result.ecode();
		this.mMessage = result.message();
	}

	public void setmMessage(String mMessage) {
		this.mMessage = mMessage;
	}

	public void setResultCode(LotteryResultCode result) {
		this.mECode = result.ecode();
		this.mMessage = result.message();
	}

	public static Result succeeded() {
		Result result = new Result();
		result.setResultCode(ResultCode.SUCCEEDED);

		return result;
	}

	public static Result succeeded(Object data) {
		Result result = new Result();
		result.setResultCode(ResultCode.SUCCEEDED);
		result.setData(data);

		return result;
	}

	public static Result failured(ResultCode resultCode) {
		Result result = new Result();
		result.setResultCode(resultCode);

		return result;
	}

	public static Result failured(LotteryResultCode resultCode) {
		Result result = new Result();
		result.setResultCode(resultCode);

		return result;
	}

	public static Result failured(ResultCode resultCode, Object data) {
		Result result = new Result();
		result.setResultCode(resultCode);
		result.setData(data);

		return result;
	}

	public void setData(Object data) {
		mData = data;
	}

	public Object getData() {
		return mData;
	}

	public String getMessage() {
		return mMessage;
	}

	public int getCode() {
		return mECode;
	}

	@Override
	public String toString() {
		return "Result [mECode=" + mECode + ", mMessage=" + mMessage + ", mData=" + mData + "]";
	}
}
