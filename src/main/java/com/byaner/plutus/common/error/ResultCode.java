package com.byaner.plutus.common.error;

import com.byaner.plutus.common.exception.UnknownDataTypeException;

/**
 * Created by bing on 2018/5/30.
 */
public enum ResultCode {
	//@formatter:off
    FAILED(-1, "Failed"),
    SUCCEEDED(0, "Succeeded"),

    /* 参数错误：10001-19999 */
    PARAM_IS_INVALID(10001, "参数无效"),
    PARAM_IS_EMPTY(10002, "参数为空"),
    PARAM_TYPE_ERROR(10003, "参数类型错误"),
    PARAM_NOT_COMPLETE(10004, "参数缺失"),

    /* 用户错误：20001-29999*/
     USER_NOT_LOGGED_IN(20001, "用户未登录"),
    USER_LOGIN_ERROR(20002, "账号不存在或密码错误"),
    USER_ACCOUNT_FORBIDDEN(20003, "账号已被禁用"),
    USER_NOT_EXIST(20004, "用户不存在"),
    USER_HAS_EXISTED(20005, "用户已存在"),

    /* 业务错误：30001-39999 */
    SPECIFIED_QUESTIONED_USER_NOT_EXIST(30001, "某业务出现问题"),
    ALREADY_COMPLETED(30002, "没有可打印票，请确认所需票处于出票成功状态"),
    PARTIAL_TICKETS(30003, "只有部分票可打印，请确认所需票处于出票成功状态"),
    PAY_FAILED(30050, "支付失败"),
    PAY_UNSUPPORTED_PAY_TYPE(30051, "不支持当前支付方式"),
    PAY_NOT_ENOUGH_MONEY(30052, "当前帐户余额不足，请换一种方式进行支付"),
    BUY_LOTTERY_FAILED(30051, "当前帐户余额不足，请换一种方式进行支付"),

    /* 系统错误：40001-49999 */
    SYSTEM_INNER_ERROR(40001, "系统繁忙，请稍后重试"),

    /* 数据错误：50001-599999 */
    DATA_NOT_FOUND(50001, "数据未找到"),
    DATA_IS_WRONG(50002, "数据有误"),
    DATA_ALREADY_EXISTED(50003, "数据已存在"),
    FILE_IS_EMPTY(50004, "数据文件为空"),
    FILE_NOT_FOUND(50005, "数据文件找不到"),

    /* 接口错误：60001-69999 */
    INTERFACE_INNER_INVOKE_ERROR(60001, "内部系统接口调用异常"),
    INTERFACE_OUTTER_INVOKE_ERROR(60002, "外部系统接口调用异常"),
    INTERFACE_FORBID_VISIT(60003, "该接口禁止访问"),
    INTERFACE_ADDRESS_INVALID(60004, "接口地址无效"),
    INTERFACE_REQUEST_TIMEOUT(60005, "接口请求超时"),
    INTERFACE_EXCEED_LOAD(60006, "接口负载过高"),

    /* 权限错误：70001-79999 */
    PERMISSION_NO_ACCESS(70001, "无访问权限"),
	NOT_IMPLEMENTED(70002, "接口未实现");

	//@formatter:on
	private int mECode;
	private String mEMsg;
	private String mEHandlingMsg;

	ResultCode(int ecode, String msg) {
		this.mECode = ecode;
		this.mEMsg = msg;
	}

	ResultCode(int ecode, String msg, String handling) {
		this.mECode = ecode;
		this.mEMsg = msg;
		this.mEHandlingMsg = handling;
	}

	public int ecode() {
		return mECode;
	}

	public String message() {
		return mEMsg;
	}

	public String handling() {
		return mEHandlingMsg;
	}

	public static ResultCode toCode(String code) throws NumberFormatException, UnknownDataTypeException {
		return toCode(Integer.parseInt(code));
	}

	public static ResultCode toCode(int code) throws UnknownDataTypeException {
		for (ResultCode r : ResultCode.values()) {
			if (r.mECode == code) {
				return r;
			}
		}
		throw new UnknownDataTypeException(ResultCode.class.getName(), "" + code);
	}

	@Override
	public String toString() {
		return "ResultCode {eCode: " + mECode + ", eMsg: " + mEMsg + ", eHandling: " + mEHandlingMsg + "}";
	}

	public static ResultCode valueOf(int ecode) {
		for (ResultCode result : ResultCode.values()) {
			if (ecode == result.ecode()) {
				return result;
			}
		}

		return null;
	}
}
