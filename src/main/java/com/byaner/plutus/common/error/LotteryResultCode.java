package com.byaner.plutus.common.error;

import com.byaner.plutus.common.exception.UnknownDataTypeException;

/**
 * Created by bing on 2018/5/20.
 */
public enum LotteryResultCode {
	/**
	 * The protocol number: #7.
	 *
	 * Define the Error codes and messages.
	 *
	 * Selling and cancel's operation
	 *
	 */
	//@formatter:off
	SUCCESS(0, "Success"),
    INVALID_1000(1000, "玩法英文名称不合法", "不扣款,检查玩法英文名称，并核对F1参数， 再重发票数据"),
    INVALID_1001(1001, "逻辑机号不合法", "不扣款,检查逻辑机号后，或者此机号无权限 重发票数据"),
    INVALID_1002(1002, "期号不合法", "不扣款,检查期号与新期参数中期号是否一致后，重发票数据"),
    INVALID_1003(1003, "注码不合法", "不扣款,检查注码是否正确(参考注码结构文档)，再重发票数据"),
    INVALID_1004(1004, "注数不正确", "不扣款,计算注码的注数不正确，参考注码结构文档"),
    INVALID_1005(1005, "校验码核对失败", "不扣款,票数据中的校验码校对不正确，请核对校验码函数与生成校验码的参数是否与合并服务器一致，再重发票数据"),
    INVALID_1006(1006, "数据传输失败，请重发", "不扣款,从调度获取IP失败，请检查中心是否下发参数，再重发票数据"),
    INVALID_1007(1007, "已期结", "不扣款,该玩法已期结，开新期后，再重发票数据"),
    INVALID_1008(1008, "注销票不存在", "不扣款,注销票不存在，检查数据是否存在"),
    INVALID_1009(1009, "该票已经注销", "不扣款,该票已经注销，不要重复注销"),
    INVALID_1010(1010, "本次销售失败，需更换流水号重新销售该票", "不扣款,换流水号后，再发票数据"),
    INVALID_1011(1011, "销售注码超过注数限制", "不扣款,注数超过限制，修改注码，减少注数后，再重新销售"),
    INVALID_1012(1012, "未到开期时间", "不扣款,检查该玩法 F1 参数的开期时间"),
    INVALID_1013(1013, "流水号非法", "不扣款,流水号必须大于 1 小于 16000;若在 此范围内，则重新换流水号后再发送票数据，保证在8秒内不重复使用流水号"),
    INVALID_1014(1014, "帐户金额不足", "不扣款,账户余额不足，请站点缴款"),
    INVALID_1015(1015, "超过注销时限", "不扣款,销售后，有个注销时限，超过了就不 能注销了"),
    INVALID_1016(1016, "彩票数据不合法", "不扣款,检查票注码与注数是否匹配，注码是否编写正确，以及命令串是否正确"),
    INVALID_1017(1017, "身份证号码错误", "不扣款,检查身份证号是否正确"),
    INVALID_1018(1018, "系统忙", "不扣款,合并服务器系统已关闭"),
    INVALID_1019(1019, "玩法没有开通", "不扣款,该玩法未开通"),
    INVALID_1030(1030, "逻辑机号正在使用", "不扣款,该机号正在使用，请稍后再销售，对于一个机号来说，属于串行处理"),
    INVALID_1040(1040, "中心忙", "不扣款,中心忙，正在做汇总"),
    INVALID_1042(1042, "超过注销限制", "不会对账户产生影响;注销时超过了单机的最大注销票数或最大注销金额"),
    INVALID_1090(1090, "解析字符串失败", "不扣款,解析命令串时，命令串不合法，解析失败，请检查命令串是否按接口文档上格式编写"),
    INVALID_1099(1099, "其它错误", "其它错误，但不会对账户产生影响"),

    /**
     * Cash's operation
     */
    INVALID_1020(1020, "玩法英文名称不合法", "兑奖不成功，玩法英文名不规范或不存在"),
    INVALID_1021(1021, "逻辑机号不合法", "兑奖不成功，逻辑机号不存在"),
    INVALID_1022(1022, "已经兑奖", "兑奖不成功，该票已经兑奖"),
    INVALID_1023(1023, "该票已经弃奖", "兑奖不成功，该票已经弃奖，不能兑奖"),
    INVALID_1024(1024, "兑奖期号不合法", "兑奖不成功，兑奖期号不合法，请核对期号"),
    INVALID_1025(1025, "兑奖金额是否正确", "兑奖不成功，兑奖金额输入不正确"),
    INVALID_1026(1026, "数据传输失败，请重新发送该票", "兑奖不成功，网络传输问题，重新兑奖"),
    INVALID_1027(1027, "已中大奖", "兑奖不成功，该票已经中大奖"),
    INVALID_1028(1028, "未中奖", "兑奖不成功，该票未中奖"),
    INVALID_CASH_1090(1090, "解析字符串失败", "兑奖不成功，兑奖命令串错误，请核对后再发兑奖命令串"),
    INVALID_1029(1029, "中心已期结", "兑奖不成功，中心已经期结"),
    INVALID_CASH_1030(1030, "逻辑机号正在使用", "兑奖不成功，逻辑机号正在使用"),
    INVALID_CASH_1040(1040, "中心忙", "不扣款,中心忙，正在做汇总"),
    INVALID_1031(1031, "玩法名称与票号不相符", "兑奖不成功，玩法名称与票号不相符"),

    /**
     * Queuing the account's balance
     */
    INVALID_QUEUE_1090(1090, "解析字符串失败", "命令串错误，请核对后再发查询命令串"),
    INVALID_QUEUE_1020(1020, "逻辑机号不合法", "查询不成功，逻辑机号不存在"),
    INVALID_QUEUE_1021(1021, "取帐户金额出错", "查询不成功，取帐户金额出错，与中心维护人员联系。"),
    INVALID_QUEUE_1030(1030, "逻辑机号正在使用", "查询不成功，逻辑机号正在使用");
	//@formatter:on
	private int mECode;
	private String mEMsg;
	private String mEHandlingMsg;

	LotteryResultCode(int ecode, String msg) {
		this.mECode = ecode;
		this.mEMsg = msg;
	}

	LotteryResultCode(int ecode, String msg, String hanlding) {
		this.mECode = ecode;
		this.mEMsg = msg;
		this.mEHandlingMsg = hanlding;
	}

	public int ecode() {
		return mECode;
	}

	public String message() {
		return mEMsg;
	}

	public String handling() {
		return mEHandlingMsg;
	}

	public static LotteryResultCode toCode(String code) throws NumberFormatException, UnknownDataTypeException {
		return toCode(Integer.parseInt(code));
	}

	public static LotteryResultCode toCode(int code) throws UnknownDataTypeException {
		for (LotteryResultCode r : LotteryResultCode.values()) {
			if (r.mECode == code) {
				return r;
			}
		}
		throw new UnknownDataTypeException(LotteryResultCode.class.getName(), "" + code);
	}

	@Override
	public String toString() {
		return "ResultCode {eCode: " + mECode + ", eMsg: " + mEMsg + ", eHandling: " + mEHandlingMsg + "}";
	}

	public static LotteryResultCode valueOf(int ecode) {
		for (LotteryResultCode result : LotteryResultCode.values()) {
			if (ecode == result.ecode()) {
				return result;
			}
		}

		return null;
	}
}
