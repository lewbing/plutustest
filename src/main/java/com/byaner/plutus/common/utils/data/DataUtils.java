package com.byaner.plutus.common.utils.data;

import com.google.gson.Gson;

import static com.byaner.plutus.common.utils.data.StringUtils.decodeUnicode;

public class DataUtils {
    public static Object resizeArray(Object oldArray, int newSize) {
        int oldSize = java.lang.reflect.Array.getLength(oldArray);
        Class elementType = oldArray.getClass().getComponentType();
        Object newArray = java.lang.reflect.Array.newInstance(elementType, newSize);

        int preserveLength = Math.min(oldSize, newSize);
        if (preserveLength > 0) {
            System.arraycopy(oldArray, 0, newArray, 0, preserveLength);
        }

        return newArray;
    }

    public static boolean isValidMobileNo(String mobiles) {
        String telRegex = "[1][3578]\\d{9}";
        // "[1]"代表第1位为数字1，"[3578]"代表第二位可以为3、5、8中的一个，"\\d{9}"代表后面是可以是0～9的数字，有9位。
        if (StringUtils.isEmpty(mobiles)) {
            return false;
        } else {
            return mobiles.matches(telRegex);
        }
    }

    public static String object2json(Object obj){
        Gson gson = new Gson();
        String obj2 = gson.toJson(obj);
        return decodeUnicode(obj2);
    }
}
