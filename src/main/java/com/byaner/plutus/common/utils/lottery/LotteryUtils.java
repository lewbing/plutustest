package com.byaner.plutus.common.utils.lottery;

import javax.xml.bind.DatatypeConverter;
import java.util.Random;
import java.util.UUID;

public class LotteryUtils {
    public static int getRandomNumber() {
        Random random = new Random();
        return random.nextInt(100000) + 899999;
    }

    public static String getRandom(int min, int max){
        Random random = new Random();
        int s = random.nextInt(max) + min;

        return String.valueOf(s);
    }

    public static String getRandomByte() {
        Random random = new Random();
        byte[] b = new byte[8];

        random.nextBytes(b);

        return DatatypeConverter.printHexBinary(b);
    }

    public static String getTicketId() {
        return getUniqueId();
    }

    public static String getUniqueId() {
        String nanoTime = String.valueOf(System.nanoTime());
        String random = LotteryUtils.getRandom(100, 1999);

        System.out.println("getUniqueId: ticketId=" + (nanoTime + random) + ", nanoTime=" + nanoTime
                + ", random=" + random);

        return (nanoTime + random);
    }

    public static String getAdditionalCode() {
        String code = "";

        for (int i = 0; i < 8; i++) {
            code += LotteryUtils.getRandom(1,9);
        }

        System.out.println("getAdditionalCode: result=" + code);

        return code;
    }

    public static String getUuid() {
        return UUID.randomUUID().toString();
    }
}
