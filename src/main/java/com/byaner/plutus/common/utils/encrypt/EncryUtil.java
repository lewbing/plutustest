package com.byaner.plutus.common.utils.encrypt;

import com.byaner.plutus.common.utils.proto8583.EncodeUtil;

public class EncryUtil {

    byte[] starC = new byte[56];
    byte[] starD = new byte[56];
    byte[][] starK = new byte[17][48];

    final int[] starip_tab = {
            58,50,42,34,26,18,10,2,
            60,52,44,36,28,20,12,4,
            62,54,46,38,30,22,14,6,
            64,56,48,40,32,24,16,8,
            57,49,41,33,25,17,9,1,
            59,51,43,35,27,19,11,3,
            61,53,45,37,29,21,13,5,
            63,55,47,39,31,23,15,7
    };

    final int[] _starip_tab = {
            40,8,48,16,56,24,64,32,
            39,7,47,15,55,23,63,31,
            38,6,46,14,54,22,62,30,
            37,5,45,13,53,21,61,29,
            36,4,44,12,52,20,60,28,
            35,3,43,11,51,19,59,27,
            34,2,42,10,50,18,58,26,
            33,1,41,9,49,17,57,25
    };

    final int[] starpc_1_c = {
            56,48,40,32,24,16,8
            ,0,57,49,41,33,25,17
            ,9,1,58,50,42,34,26
            ,18,10,2,59,51,43,35
    };

    final int[] starpc_1_d = {
            62,54,46,38,30,22,14
            ,6,61,53,45,37,29,21
            ,13,5,60,52,44,36,28
            ,20,12,4,27,19,11,3
    };

    final int[] starpc_2 = {
            14,17,11,24,1,5,
            3,28,15,6,21,10,
            23,19,12,4,26,8,
            16,7,27,20,13,2,
            41,52,31,37,47,55,
            30,40,51,45,33,48,
            44,49,39,56,34,53,
            46,42,50,36,29,32
    };

    final int[] starls_count = {
            1,1,2,2,2,2,2,2,1,2,2,2,2,2,2,1
    };

    final int[] stare_r = {
            32,1,2,3,4,5,4,5,6,7,8,9,
            8,9,10,11,12,13,12,13,14,15,16,17,
            16,17,18,19,20,21,20,21,22,23,24,25,
            24,25,26,27,28,29,28,29,30,31,32,1
    };

    final int[] starP = {
            16,7,20,21,29,12,28,17,
            1,15,23,26,5,18,31,10,
            2,8,24,14,32,27,3,9,
            19,13,30,6,22,11,4,25
    };

    final int[][][] starSSS = {
            {
                    {14,4,13,1,2,15,11,8,3,10,6,12,5,9,0,7},
                    {0,15,7,4,14,2,13,1,10,6,12,11,9,5,3,8},
                    {4,1,14,8,13,6,2,11,15,12,9,7,3,10,5,0},
                    {15,12,8,2,4,9,1,7,5,11,3,14,10,0,6,13}
            },

            {
                    {15,1,8,14,6,11,3,4,9,7,2,13,12,0,5,10},
                    {3,13,4,7,15,2,8,14,12,0,1,10,6,9,11,5},
                    {0,14,7,11,10,4,13,1,5,8,12,6,9,3,2,15},
                    {13,8,10,1,3,15,4,2,11,6,7,12,0,5,14,9}
            },

            {
                    {10,0,9,14,6,3,15,5,1,13,12,7,11,4,2,8},
                    {13,7,0,9,3,4,6,10,2,8,5,14,12,11,15,1},
                    {13,6,4,9,8,15,3,0,11,1,2,12,5,10,14,7},
                    {1,10,13,0,6,9,8,7,4,15,14,3,11,5,2,12}
            },

            {
                    {7,13,14,3,0,6,9,10,1,2,8,5,11,12,4,15},
                    {13,8,11,5,6,15,0,3,4,7,2,12,1,10,14,9},
                    {10,6,9,0,12,11,7,13,15,1,3,14,5,2,8,4},
                    {3,15,0,6,10,1,13,8,9,4,5,11,12,7,2,14}
            },

            {
                    {2,12,4,1,7,10,11,6,8,5,3,15,13,0,14,9},
                    {14,11,2,12,4,7,13,1,5,0,15,10,3,9,8,6},
                    {4,2,1,11,10,13,7,8,15,9,12,5,6,3,0,14},
                    {11,8,12,7,1,14,2,13,6,15,0,9,10,4,5,3}
            },

            {
                    {12,1,10,15,9,2,6,8,0,13,3,4,14,7,5,11},
                    {10,15,4,2,7,12,9,5,6,1,13,14,0,11,3,8},
                    {9,14,15,5,2,8,12,3,7,0,4,10,1,13,11,6},
                    {4,3,2,12,9,5,15,10,11,14,1,7,6,0,8,13}
            },

            {
                    {4,11,2,14,15,0,8,13,3,12,9,7,5,10,6,1},
                    {13,0,11,7,4,9,1,10,14,3,5,12,2,15,8,6},
                    {1,4,11,13,12,3,7,14,10,15,6,8,0,5,9,2},
                    {6,11,13,8,1,4,10,7,9,5,0,15,14,2,3,12}
            },

            {
                    {13,2,8,4,6,15,11,1,10,9,3,14,5,0,12,7},
                    {1,15,13,8,10,3,7,4,12,5,6,11,0,14,9,2},
                    {7,11,4,1,9,12,14,2,0,6,10,13,15,3,5,8},
                    {2,1,14,7,4,10,8,13,15,12,9,0,3,5,6,11}
            }
    };

    final byte[][] bit_box = {
            {0,0,0,0},
            {0,0,0,1},
            {0,0,1,0},
            {0,0,1,1},
            {0,1,0,0},
            {0,1,0,1},
            {0,1,1,0},
            {0,1,1,1},
            {1,0,0,0},
            {1,0,0,1},
            {1,0,1,0},
            {1,0,1,1},
            {1,1,0,0},
            {1,1,0,1},
            {1,1,1,0},
            {1,1,1,1}
    };

    public byte[] _DES(byte[] mtext,byte[] key)
    {
        byte[] tmp;
        byte[] text = null;

        try {
            tmp = starexpand0(key);
            setkeystar(tmp);
            text = stardiscrypt0(mtext);
        }catch (Exception e) {
            e.printStackTrace();
        }

        return text;
    }

    public byte[] DES_(byte[] mtext,byte[] key)
    {
        byte[] tmp;
        byte[] text = null;

        try {
            tmp = starexpand0(key);
            setkeystar(tmp);

            text = starencrypt0(mtext);
        }catch (Exception e) {
            e.printStackTrace();
        }

        return text;
    }

    private byte[] stardiscrypt0(byte[] mtext)
    {
        byte[] text = null;
        byte[] tmp = null;
        int i;
        try {
            StartIP startIP =  starip(mtext);

            for (i=16;i>0;i--) {
                startIP = starF(i,startIP.ll,startIP.rr,startIP.LL,startIP.RR);
                StartIP startIPTmp = starF(--i,startIP.LL,startIP.RR,startIP.ll,startIP.rr);
                startIP.LL = startIPTmp.ll;
                startIP.RR = startIPTmp.rr;
                startIP.rr = startIPTmp.RR;
                startIP.ll = startIPTmp.LL;
            }

            tmp = _starip(startIP.rr,startIP.ll);

            text = starcompress0(tmp);
        }catch (Exception e) {
            e.printStackTrace();
        }
        return text;
    }


    private byte[] starencrypt0(byte[] mtext)
    {
        byte[] text = null;

        byte[] tmp = null;
        int i;
        try {
            StartIP startIP =  starip(mtext);

            for (i=1;i<17;i++) {
                startIP = starF(i,startIP.ll,startIP.rr,startIP.LL,startIP.RR);
                StartIP startIPTmp = starF(++i,startIP.LL,startIP.RR,startIP.ll,startIP.rr);
                startIP.LL = startIPTmp.ll;
                startIP.RR = startIPTmp.rr;
                startIP.rr = startIPTmp.RR;
                startIP.ll = startIPTmp.LL;
            }

            tmp = _starip(startIP.rr,startIP.ll);

            text = starcompress0(tmp);
        }catch (Exception e) {
            e.printStackTrace();
        }
        return text;
    }



    private byte[] starcompress0(byte[] out)
    {
        int i,j;

        byte[] in = new byte[8];
        try {
            int pos = 0;

            for (i=0;i<8;i++) {
                for (in[i]=0,j=7;j>=0;j--)
                    in[i]+=(out[pos++])<<j;
            }
        }catch (Exception e) {
            e.printStackTrace();
        }
        return in;
    }


    private byte[] starexpand0(byte[] in)
    {
        int i,j;
        byte[] out = new byte[64];
        int pos = 0;

        try {
            for (i=0;i<8;i++) {
                for (j=7;j>=0;j--) {
                    out[pos++]=(byte)((in[i]>>j)&1);
                }
            }
        }catch (Exception e) {
            e.printStackTrace();
        }
        return out;
    }

    private void setkeystar(byte[] bits)
    {
        int i,j;
       try {
           for (i=0;i<28;i++) {
               starC[i]=starC[i+28]=bits[starpc_1_c[i]];
               starD[i]=starD[i+28]=bits[starpc_1_d[i]];
           }
           for (i=0,j=0;j<16;j++) {
               i+=starls_count[j];
               byte[] starCTmp = new byte[starC.length-i];
               byte[] starDTmp = new byte[starD.length-i];
               System.arraycopy(starC,i,starCTmp,0,starCTmp.length);
               System.arraycopy(starD,i,starDTmp,0,starDTmp.length);
               starK[j+1] = starson(starCTmp,starDTmp);
           }
       }catch (Exception e) {
           e.printStackTrace();
       }
    }

    private byte[] starson(byte[] cc,byte[] dd)
    {
        int i;
        byte[] buffer = new byte[56];
        byte[] kk = new byte[48];
        int pos = 0;

        try {
            System.arraycopy(cc,0,buffer,0,28);
            System.arraycopy(dd,0,buffer,28,28);
            for (i=0;i<48;i++) {
                kk[pos++] = buffer[starpc_2[i] - 1];
            }

        }catch (Exception e) {
            e.printStackTrace();
        }
        return kk;
    }

    private StartIP starip(byte[] text)
    {
        int i;
        StartIP startIP = new StartIP();

        try {
            byte[] buffer = starexpand0(text);

            for (i=0;i<32;i++) {
                startIP.ll[i]=buffer[starip_tab[i]-1];
                startIP.rr[i]=buffer[starip_tab[i+32]-1];
            }
        }catch (Exception e) {
            e.printStackTrace();
        }
        return startIP;
    }

    private StartIP starF(int n,byte[] ll, byte[] rr, byte[] LL, byte[] RR) {

        StartIP result = new StartIP();
        int i;
        byte[] buffer = new byte[64];
        byte[] tmp = null;

        try {
            for (i=0;i<48;i++)
                buffer[i]=(byte) (rr[stare_r[i]-1]^starK[n][i]);

            tmp = stars_box(buffer);

            for (i=0;i<32;i++)
                RR[i]=(byte) (tmp[starP[i]-1]^ll[i]);

            System.arraycopy(rr,0,LL,0,32);

            result.rr = rr;
            result.ll = ll;
            result.LL = LL;
            result.RR = RR;
        }catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    private byte[] stars_box(byte[] aa)
    {
        int i,j,m,y,z;
        byte[] bb = new byte[64];

        try {
            for (i=0,m=0,j=0;i<8;i++,j+=6) {
                y=(aa[j]<<1)+aa[j+5];
                z=(aa[j+1]<<3)+(aa[j+2]<<2)+(aa[j+3]<<1)+aa[j+4];
                System.arraycopy(bit_box[ starSSS[i][y][z] ],0,bb,m,4);
                m+=4;
            }
        }catch (Exception e) {
            e.printStackTrace();
        }
        return bb;
    }

    private byte[] _starip(byte[] ll,byte[] rr)
    {
        int i;
        byte[] tmp = new byte[64];
        byte[] text = new byte[64];
        try {
            System.arraycopy(ll,0,tmp,0,32);
            System.arraycopy(rr,0,tmp,32,32);
            for (i=0;i<64;i++)
                text[i]=tmp[_starip_tab[i]-1];
        }catch (Exception e) {
            e.printStackTrace();
        }
        return text;
    }



    private byte[] MyXor(byte[] puszStr1, byte[] puszStr2, int uiNum)
    {
        int i;
        try {
            for(i=0; i<uiNum; i++)
                puszStr1[i]^= puszStr2[i];
        }catch (Exception e) {
            e.printStackTrace();
        }
        return puszStr1;
    }


    public byte[] ansi99DesMac(byte[] puszMsg,byte[] puszKey)
    {
        byte[] uszTmp = new byte[8];
        byte[] puszMac = new byte[8];
        byte[] xorPuszMsg = null;
        int iFlg = 0;
        int iCount;
        int i;
        int iMsgLen = puszMsg.length;

        try {
            iFlg = iMsgLen%8;
            if (iFlg != 0)
                iCount = iMsgLen / 8 + 1;
            else
                iCount = iMsgLen / 8;
            for (i=0; i<iCount-1; i++)
            {
                xorPuszMsg = new byte[8];
                System.arraycopy(puszMsg,(i << 3),xorPuszMsg,0,8);
                puszMac = MyXor(puszMac, xorPuszMsg , 8);
                puszMac = DES_(puszMac,puszKey);
            }

            if (iFlg == 0) {
                xorPuszMsg = new byte[8];
                System.arraycopy(puszMsg, ((iCount - 1) << 3), xorPuszMsg, 0, 8);
                puszMac = MyXor(puszMac, xorPuszMsg, 8);
            }else
            {
                System.arraycopy(puszMsg,((iCount-1)<<3),uszTmp,0,iFlg);
                puszMac = MyXor(puszMac, uszTmp, 8);
            }
            puszMac = DES_(puszMac,puszKey);
        }catch (Exception e) {
            e.printStackTrace();
        }

        return puszMac;
    }

    public class  StartIP {
        byte[] ll = new byte[64];
        byte[] rr = new byte[64];
        byte[] LL = new byte[64];
        byte[] RR = new byte[64];
    }

    public String dataEncrypt(String str) {

        try {
            byte[] outData = new byte[2048];
            int iOutLen = 0;
            int iLen;
            byte[] KEY = new byte[8];
            for (int i=0; i<8; i++) {
                KEY[i] = (byte) 0x88;
            }
            str = EncodeUtil.padLeft(String.valueOf(str.length()),4,"0") + str;
            byte[] strByte = str.getBytes("ISO8859-1");
            byte[] encrypt = ansi99DesMac(strByte,KEY);
            byte[] indata = new byte[strByte.length + 4];
            System.arraycopy(strByte,0,indata,0,strByte.length);
            System.arraycopy(encrypt,0,indata,strByte.length,4);

            iLen = indata.length;
            for (int i=0; i<(iLen+8+7)/8; i++) {
                int tmpLen = iLen - i*8;
                if (tmpLen > 0) {
                    byte[] tmp = new byte[tmpLen];
                    System.arraycopy(indata,i*8,tmp,0,tmpLen);
                    byte[] entmp = DES_(tmp,KEY);
                    System.arraycopy(entmp,0,outData,iOutLen,entmp.length);
                    iOutLen += entmp.length;
                }
            }

            byte[] data = new byte[iOutLen];
            System.arraycopy(outData,0,data,0,iOutLen);
            return EncodeUtil.hex(data);

        }catch (Exception e) {
            e.printStackTrace();
        }

        return "";

    }

}
