package com.byaner.plutus.common.utils.proto8583;

import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.*;


public class CN8583Utils {

    public static String packet_encoding="GBK";//报文编码 UTF-8 GBK
    private static Map map8583Definition = null;// 8583报文128域定义器
    private static String BODYFILEDMAP = "bodyFiledMap";
    private static String BIT64FILEDMAP = "bit64FiledMap";
    static {
        Properties property = new Properties();
        InputStream fis;
        try {
			ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
			fis = classLoader.getResourceAsStream("config_8583.properties");
			property.load(fis);
			CN8583Utils.map8583Definition = new HashMap(property);
			fis.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // 8583报文初始位图:128位01字符串
    public static String getInitBitMap(){
        String initBitMap =
                "00000000" + "00000000" + "00000000" + "00000000"
                        + "00000000" + "00000000" + "00000000" + "00000000";
        return initBitMap;
    }

    /**
     * 组装8583报文
     * @param hm
     * @return
     */
    public static byte[] make8583(TreeMap filedMap){
        byte[] whoe8583=null;
        if(filedMap==null){
            return null;
        }
        try {
            String  bitMap64=getInitBitMap();//获取初始化的128位图
            //按照8583定义器格式化各个域的内容
            Map all=formatValueTo8583(filedMap,bitMap64);
            // 获取上送报文内容
            whoe8583=getWhole8583Packet(all);
            return whoe8583;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return whoe8583;
    }
    /**
     * 获取完整的8583报文体（128域）
     * @param all
     * @return
     */
    public static byte[] getWhole8583Packet(Map all){
        if(all==null||all.get(BODYFILEDMAP)==null||all.get(BIT64FILEDMAP)==null){
            return null;
        }
        try {
            String  bitMap64=(String)all.get(BIT64FILEDMAP);
            // 64域位图二进制字符串转8位16进制
            byte[] bitmaps= get8BitByteFromStr(bitMap64);

            TreeMap pacBody=(TreeMap)all.get(BODYFILEDMAP);
            Iterator it=pacBody.keySet().iterator();
            byte[] bitContent = null;
            for(;it.hasNext();){
                String key=(String)it.next();
                String value=(String)pacBody.get(key);
                bitContent = CN8583Utils.arrayApend(bitContent, value.getBytes("ISO8859-1"));
            }

            //组装
            byte[] package8583=null;
            package8583= CN8583Utils.arrayApend(package8583, bitmaps);
            package8583= CN8583Utils.arrayApend(package8583, bitContent);

            return package8583;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static Map formatValueTo8583(TreeMap filedMap,String  bitMap64){
        Map all=new HashMap();
        TreeMap formatedFiledMap=new TreeMap();//格式化结果
        if(filedMap!=null){
            Iterator it=filedMap.keySet().iterator();
            for(;it.hasNext();){
                String fieldName=(String)it.next();//例如FIELD005
                String fieldValue=(String)filedMap.get(fieldName);//字段值

                try{
                    if (fieldValue == null) {
                        System.out.println("error:报文域 {" + fieldName + "}为空值");
                        fieldValue = "";
                        return null;
                    }
                    // 数据域名称FIELD开头的为64域
                    if (fieldName.startsWith("FIELD") && fieldValue.length() >= 0) {
                        String fieldNo = fieldName.substring(5, 8);//例如005
                        // 组二进制位图串
                        bitMap64 = change8bitMapFlag(fieldNo, bitMap64);

                        // 获取域定义信息
                        String[] fieldDef = CN8583Utils.map8583Definition.get("FIELD" + fieldNo).toString().split(",");
                        String fieldType=fieldDef[0];//类型定义例string
                        String strfieldMaxLen=fieldDef[1];//长度定义,例20
                        boolean isFixLen=true;//是否定长判断

                        if(strfieldMaxLen.startsWith("VAR")){//变长域
                            isFixLen=false;
                            strfieldMaxLen=strfieldMaxLen.substring(3);//获取VAR2后面的2
                        }

                        int fieldLen = 0;
                        // 判断是否为变长域
                        if (!isFixLen) {// 变长域(变长域最后组装成的效果：例如变长3位，定义var3，这里的3是指长度值占3位，字段值是123456，最后结果就是006123456)
                            byte [] fieldByteLen = null;
                            byte [] fieldByteValue = null;
                            if(fieldType.startsWith("banary")) {

                                fieldLen = fieldValue.getBytes("ISO8859-1").length;
                                if (fieldLen > Integer.valueOf(strfieldMaxLen)) {
                                    return null;
                                }
                                fieldByteLen = EncodeUtil.bcd(fieldLen,2);
                                fieldByteValue = arrayApend(fieldByteLen,fieldValue.getBytes("ISO8859-1"));
                            }else if(fieldType.startsWith("bcd")){
                                fieldLen = fieldValue.getBytes("ISO8859-1").length;
                                if (fieldLen > Integer.valueOf(strfieldMaxLen)) {
                                    return null;
                                }

                                if (fieldLen%2 != 0){
                                    fieldValue =EncodeUtil.addBlankRight(fieldValue,1,"0");
                                }
                                fieldByteLen = EncodeUtil.bcd(fieldLen,2);
                                fieldByteValue = arrayApend(fieldByteLen,EncodeUtil.bcd(fieldValue));

                            }else {
                                fieldLen = fieldValue.getBytes(packet_encoding).length;
                                fieldByteLen = EncodeUtil.bcd(fieldLen,2);
                                fieldByteValue = arrayApend(fieldByteLen,fieldValue.getBytes(packet_encoding));
                            }
                            fieldValue = new String(fieldByteValue, "ISO8859-1");

                        } else {//定长域(定长比较好理解，一个字段规定是N位，那么字段值绝对不能超过N位，不足N位就在后面补空格)
                            if(fieldType.startsWith("banary")) {
                                fieldLen = fieldValue.getBytes("ISO8859-1").length;
                                if (fieldLen > Integer.valueOf(strfieldMaxLen)) {
                                    return null;
                                }
                            }else if(fieldType.startsWith("bcd")) {
                                fieldLen = fieldValue.getBytes("ISO8859-1").length;
                                if (fieldLen > Integer.valueOf(strfieldMaxLen)) {
                                    return null;
                                }
                                fieldValue = new String(EncodeUtil.bcd(fieldValue),"ISO8859-1");
                            }else {
                                fieldLen = fieldValue.getBytes("ISO8859-1").length;
                                if (fieldLen > Integer.valueOf(strfieldMaxLen)) {
                                    return null;
                                }
                                fieldValue=getFixFieldValue(fieldValue,Integer.valueOf(strfieldMaxLen));//定长处理
                            }

                            fieldValue = new String(fieldValue.getBytes("ISO8859-1"),"ISO8859-1");
                        }
                        //System.out.println("组装后报文域 {" + fieldName + "}==" + fieldValue+"==,域长度:"+fieldValue.getBytes(packet_encoding).length);
                    }

                    // 返回结果赋值
                    if (filedMap.containsKey(fieldName)) {
                        if (formatedFiledMap.containsKey(fieldName)) {
                            formatedFiledMap.remove(fieldName);
                        }
                        formatedFiledMap.put(fieldName, fieldValue);
                    } else {
                        // System.out.println("error:" +fieldName + "配置文件中不存在!");
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }//end for
        }
        System.out.println("");
        all.put(BODYFILEDMAP, formatedFiledMap);
        all.put(BIT64FILEDMAP, bitMap64);
        return all;
    }



    /**
     * 解析8583报文
     *
     * @param content8583
     */
    public static Map analyze8583(byte[] content8583) {
        TreeMap filedMap=new TreeMap();
        try {
            // 取位图
            byte[] bitMap8byte = new byte[8];
            System.arraycopy(content8583, 0, bitMap8byte, 0, 8);
            // 8位图转2进制位图64位字符串
            String bitMap64Str = get8BitMapStr(bitMap8byte);

            //记录当前位置,从位图后开始遍历取值
            int pos = 8;
            // 遍历128位图，取值。注意从FIELD002开始
            for (int i = 1; i < bitMap64Str.length(); i++) {
                String filedValue = "";//字段值
                String filedName = "FIELD" + getNumThree((i+1));//FIELD005

                if (bitMap64Str.charAt(i) == '1') {
                    // 获取域定义信息
                    String[] fieldDef = CN8583Utils.map8583Definition.get(filedName).toString().split(",");
                    String defType=fieldDef[0];//类型定义例string
                    String defLen=fieldDef[1];//长度定义,例20
                    boolean isFixLen=true;//是否定长判断

                    if(defLen.startsWith("VAR")){//变长域
                        isFixLen=false;
                        defLen=defLen.substring(3);//获取VAR2后面的2
                    }
                    // 截取该域信息
                    if (!isFixLen) {//变长域

                        byte [] defLenByte = new byte[2];
                        defLenByte[0] = content8583[pos];
                        defLenByte[1] = content8583[pos+1];

                        int realLen1 = Integer.valueOf(EncodeUtil.hex(defLenByte));

                        if(defType.startsWith("bcd")) {
                            realLen1 = (realLen1%2 == 0) ? realLen1/2 : realLen1/2+1;
                        }

                        int realAllLen=realLen1+2;//该字段总长度（包括长度值占的长度2）

                        byte[] filedValueByte=new byte[realLen1];
                        System.arraycopy(content8583, pos+2, filedValueByte, 0, filedValueByte.length);

                        byte[] tmp1 = null;
                        byte[] tmp2 = null;
                        if (defType.startsWith("binary")) {
                            filedValue=new String(filedValueByte,"ISO8859-1");
                        }else if(defType.startsWith("bcd")) {
                            filedValue=EncodeUtil.hex(filedValueByte);
                        }else
                        {
                            filedValue=new String(filedValueByte,packet_encoding);
                        }

                        pos += realAllLen;//记录当前位置
                    } else {//定长域
                        int defLen2 = Integer.valueOf(defLen);//长度值占的位数
                        if(defType.startsWith("bcd")) {
                            defLen2 = defLen2/2;
                        }

                        byte[] filedValueByte=new byte[defLen2];
                        System.arraycopy(content8583, pos, filedValueByte, 0, filedValueByte.length);

                        if (defType.startsWith("binary")) {
                            filedValue=new String(filedValueByte,"ISO8859-1");
                        }else if(defType.startsWith("bcd")) {
                            filedValue=EncodeUtil.hex(filedValueByte);
                        }else
                        {
                            int num = 0;
                            for (int k = filedValueByte.length-1; k>=0; k--) {
                                byte c = filedValueByte[k];
                                if (c == 0x00 || c == 0x20) {
                                    num ++;
                                    continue;
                                }
                                break;
                            }

                            byte[] dealFiledValueByte = new byte[filedValueByte.length - num];
                            System.arraycopy(filedValueByte,0,dealFiledValueByte,0,dealFiledValueByte.length);

                            filedValue=new String(dealFiledValueByte,packet_encoding);

                        }
                        pos += defLen2;//记录当前位置
                    }
                    filedMap.put(filedName, filedValue);
                }
            }//end for
        } catch (Exception e) {
            e.printStackTrace();
        }
        return filedMap;
    }

    //********************************以下是工具方法,有些没有使用到***********************************************************//

    /**
     * 复制字符
     * @param str
     * @param count
     * @return
     */
    public static String strCopy(String str,int count){
        StringBuffer sb = new StringBuffer();
        for(int i=0;i < count;i++){
            sb.append(str);
        }
        return sb.toString();
    }
    /**
     * 将setContent放入set（考虑到数组越界）
     * @param set
     * @param setContent
     * @return
     */
    public static byte[] setToByte(byte[] set,byte[] setContent){
        byte[] res=new byte[set.length];
        if(set==null||setContent==null){

        }else{
            if(set.length<setContent.length){

            }else{
                System.arraycopy(setContent, 0, res, 0, setContent.length);
            }
        }
        return res;
    }
    public static byte[] setToByte(byte[] set,String setContentStr){
        byte[] res=new byte[set.length];
        byte[] setContent;
        try {
            setContent = setContentStr.getBytes(packet_encoding);
            res=setToByte(res,setContent);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return res;
    }

    public static String getPacketLen(int len){
        String res="";
        String lenStr=String.valueOf(len);
        int lenC=4-lenStr.length();
        res=strCopy("0",lenC)+lenStr;
        return res;
    }
    public static String getPacketLen(String lenStr){
        String res="";
        if(lenStr==null){

        }else{
            res=getPacketLen(Integer.valueOf(lenStr));
        }
        return res;
    }


    /**
     * 返回a和b的组合,实现累加功能
     * @param a
     * @param b
     * @return
     */
    public static byte[] arrayApend(byte[] a,byte[] b){
        int a_len=(a==null?0:a.length);
        int b_len=(b==null?0:b.length);
        byte[] c=new byte[a_len+b_len];
        if(a_len==0&&b_len==0){
            return null;
        }else if(a_len==0){
            System.arraycopy(b, 0, c, 0, b.length);
        }else if(b_len==0){
            System.arraycopy(a, 0, c, 0, a.length);
        }else{
            System.arraycopy(a, 0, c, 0, a.length);
            System.arraycopy(b, 0, c, a.length, b.length);
        }
        return c;
    }


    /**
     * 改变64位图中的标志为1
     * @param fieldNo
     * @param res
     * @return
     */
    public static String change8bitMapFlag(String fieldNo, String res) {
        int indexNo=Integer.parseInt(fieldNo);
        res = res.substring(0, indexNo-1) + "1" + res.substring(indexNo);
        return res;
    }


    /**
     * 位图操作
     *
     * 把8位图的字节数组转化成64位01字符串
     * @param packet_header_map
     * @return
     */
    public static String get8BitMapStr(byte[] bitMap8){
        String bitMap64 = "";
        // 16位图转2进制位图128位字符串
        for (int i = 0; i < bitMap8.length; i++) {
            int bc = bitMap8[i];
            bc=(bc<0)?(bc+256):bc;
            String bitnaryStr=Integer.toBinaryString(bc);//二进制字符串
            // 左补零，保证是8位
            String rightBitnaryStr = strCopy("0",Math.abs(8-bitnaryStr.length())) + bitnaryStr;//位图二进制字符串
            // 先去除多余的零，然后组装64域二进制字符串
            bitMap64+=rightBitnaryStr;
        }
        return bitMap64;
    }

    /**
     *  位图操作
     *
     * 把128位01字符串转化成16位图的字节数组
     * @param packet_header_map
     * @return
     */
    public static byte[] get8BitByteFromStr(String str_64){
        byte[]  bit16=new byte[8];
        try {
            if(str_64==null||str_64.length()!=64){
                return null;
            }
            // 64域位图二进制字符串转8位16进制
            byte[]  tmp=str_64.getBytes(packet_encoding);
            int weight;//权重
            byte[] strout = new byte[64];
            int i, j, w = 0;
            for (i = 0; i < 8; i++) {
                weight = 0x0080;
                for (j = 0; j < 8; j++) {
                    strout[i] += ((tmp[w]) - '0') * weight;
                    weight /= 2;
                    w++;
                }
                bit16[i] = strout[i];
            }
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return bit16;
    }


    /**
     * 从完整的8583报文中获取位图（8字节数组）
     * @param packet
     * @return
     */
    public static byte[] getPacketHeaderMap(byte[] packet){
        byte[] packet_header_map = new byte[8];
        if(packet==null||packet.length<8){
            return null;
        }
        for(int i=0;i<8;i++){
            packet_header_map[i]=packet[i];
        }
        return packet_header_map;
    }
    /**
     * 从完整的8583报文中获取8位图，转化成64位的01字符串
     *
     * @param content8583
     * @return
     */
    public static String get8BitMapFrom8583Byte(byte[] content8583){
        // 取位图
        byte[] bitMap8 = getPacketHeaderMap(content8583);
        // 16位图转2进制位图128位字符串
        String bitMap64 = get8BitMapStr(bitMap8);

        return bitMap64;
    }



    //返回字段号码，例如005
    public static String getNumThree(int i){
        String len="";
        String iStr=String.valueOf(i);
        len=strCopy("0",3-iStr.length())+iStr;
        return len;
    }

    /**
     * 将字段值做定长处理，不足定长则在后面补空格
     * @param valueStr
     * @param defLen
     * @return
     */
    public static String getFixFieldValue(String valueStr,int defLen){
        return getFixFieldValue(valueStr,defLen,packet_encoding);
    }
    public static String getFixFieldValue(String valueStr,int defLen,String encoding){
        String fixLen="";
        try {
            if(valueStr==null){
                return strCopy(" ",defLen);
            }else{
                byte[] valueStrByte=null;
                //这里最好指定编码，不使用平台默认编码
                if(encoding==null||encoding.trim().equals("")){
                    valueStrByte=valueStr.getBytes();
                }else{
                    valueStrByte=valueStr.getBytes(encoding);
                }
                //长度的判断使用转化后的字节数组长度，因为中文在不同的编码方式下，长度是不同的，GBK是2，UTF-8是3，按字符创长度算就是1.
                //解析报文是按照字节来解析的，所以长度以字节长度为准，防止中文带来乱码
                if(valueStrByte.length>defLen){
                    return null;
                }else{
                    fixLen=valueStr+strCopy(" ",defLen-valueStrByte.length);
                }
            }
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        return fixLen;
    }


    public static String getPacket_encoding() {
        return packet_encoding;
    }

    public static void setPacket_encoding(String packet_encoding) {
        CN8583Utils.packet_encoding = packet_encoding;
    }

    public static Map getMap8583Definition() {
        return map8583Definition;
    }

    public static void setMap8583Definition(Map map8583Definition) {
        CN8583Utils.map8583Definition = map8583Definition;
    }


}
