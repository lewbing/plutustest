package com.byaner.plutus.common.utils.lottery;

import com.byaner.plutus.common.constants.Constants;

/**
 * Created by bing on 2018/6/28.
 */
public class BonusUtils {
    public static final int MAX_SHORT_PERIOD_BONUS = 50;
    public static final int NAX_LONG_PERIOD_BOUNS = 30;

    public static int getMaxBonusNumber(String gameName) {
        if (Constants.GameNames.QUICK_THREE.equals(gameName)) {
            return MAX_SHORT_PERIOD_BONUS;
        }

        return NAX_LONG_PERIOD_BOUNS;
    }
}
