package com.byaner.plutus.common.utils;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by bing on 2018/9/3.
 */
public class DateUtils {
    /**
     * 获取可阅读时间格式，并以秒为单位
     *
     * @return
     */
    public static String getDateTime() {
        //设置日期格式
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        String result = df.format(new Date());

        System.out.println("getDateTime: result=" + result);

        return result;
    }

    /**
     * 获取可阅读时间格式，并以毫秒为单位
     *
     * @return
     */
    public static String getDateTimeMills() {
        //设置日期格式
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss:SSSS");

        String result = df.format(new Date());

        System.out.println("getDateTime: result=" + result);

        return result;
    }
}
