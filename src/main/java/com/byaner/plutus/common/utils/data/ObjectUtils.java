package com.byaner.plutus.common.utils.data;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import net.sf.json.JSONObject;

import java.beans.BeanInfo;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.HashMap;
import java.util.Map;

public class ObjectUtils {
    public static <T> T json2Object(String json, Class<T> objectClass) {
        if (json == null || json.isEmpty()) {
            return null;
        }

//        Gson gs = new Gson();
        Gson gs = new GsonBuilder().serializeNulls().create();
        T result = null;

        try {
            result = gs.fromJson(json, objectClass);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    public static String object2Json(Object jsonObject) {
        JSONObject resultObject = JSONObject.fromObject(jsonObject);

        String result = resultObject.toString();

        return result;
    }

    /**
     * JavaBean对象转化成Map对象
     *
     * @param javaBean
     * @return
     * @author jqlin
     */
    @SuppressWarnings({ "rawtypes", "unchecked" })
    public static Map object2Map(Object javaBean) {
        Map map = new HashMap();

        try {
            // 获取javaBean属性
            BeanInfo beanInfo = Introspector.getBeanInfo(javaBean.getClass());

            PropertyDescriptor[] propertyDescriptors = beanInfo.getPropertyDescriptors();
            if (propertyDescriptors != null && propertyDescriptors.length > 0) {
                String propertyName = null; // javaBean属性名
                Object propertyValue = null; // javaBean属性值
                for (PropertyDescriptor pd : propertyDescriptors) {
                    propertyName = pd.getName();
                    if (!propertyName.equals("class")) {
                        Method readMethod = pd.getReadMethod();
                        propertyValue = readMethod.invoke(javaBean, new Object[0]);
                        map.put(propertyName, propertyValue);
                    }
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return map;
    }

    public static <T> T map2Object(Map map, Class<?> beanClass) {
        if (map == null) {
            return null;
        }

        T result = null;

        try {
            result = (T) beanClass.newInstance();
            Field[] fields = result.getClass().getDeclaredFields();
            for (Field field : fields) {
                int mod = field.getModifiers();
                if(Modifier.isStatic(mod) || Modifier.isFinal(mod)){
                    continue;
                }
                field.setAccessible(true);
                field.set(result, map.get(field.getName()));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    public static String map2Json(Map map) {
        JSONObject jsonObject = JSONObject.fromObject(map);

        return jsonObject.toString();
    }
}
