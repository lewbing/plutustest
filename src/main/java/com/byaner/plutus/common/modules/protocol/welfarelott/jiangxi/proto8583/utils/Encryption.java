package com.byaner.plutus.common.modules.protocol.welfarelott.jiangxi.proto8583.utils;

import java.io.UnsupportedEncodingException;
import java.util.Arrays;

/**
 * Created by bing on 2018/5/28.
 */
public class Encryption {
    private static final String DEF_STRING_CHARSET = "GB18030";

    /**
     * 对彩票数据内容进行加密
     *
     * @param sString
     * @return
     */
    public static String encode2String(String sString) {
        byte[] bytes = sString.getBytes();
        int len = bytes.length;

        for (int i = 0; i < len; i++) {
            byte data = bytes[i];
            data = (byte)(data ^ 0x9f);
            data = (byte)((data >> 4) | ((data & 0xf) << 4));
            bytes[i] = data;
        }

        try {
            return new String(bytes, DEF_STRING_CHARSET);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        return null;
    }

    /**
     * 对彩票数据内容进行加密
     *
     * @param sString 需要加密的ASCII字串
     *
     * @return 加密成功后的二进制字串
     */
    public static byte[] encode2Bytes(String sString) {
        byte[] buff = new byte[400];
        Arrays.fill(buff, (byte) 0);

        byte[] bytes = sString.getBytes();
        int len = bytes.length;

        for (int i = 0; i < len; i++) {
            int n = bytes[i];
            n = n ^ 0x09f;
            n = ((n >> 4) & 0x0f) | ((n & 0x0f) << 4);
            buff[i] = (byte)n;
        }

        return buff;
    }

    /**
     * 对彩票数据内容进行解密
     *
     * @param sString 需要解密的二进制字串
     *
     * @return 解析完成后的ASCII数据
     */
    public static String decode(String sString) {
        byte[] bytes = sString.getBytes();
        int len = bytes.length;

        for (int i = 0; i < len; i++) {
            int ch = bytes[i];
            ch = ((ch >> 4) & 0x0f) | ((ch & 0x0f) << 4);
            ch = ch ^ 0x09f;
            bytes[i] = (byte)ch;
        }

        try {
            return new String(bytes, DEF_STRING_CHARSET);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        return null;
    }

    /**
     * 对彩票数据内容进行解密
     *
     * @param bytes 需要解密的二进制数据
     *
     * @return 解析完成后的ASCII数据
     */
    public static String decode(byte[] bytes) {
        int len = bytes.length;

        for (int i = 0; i < len; i++) {
            int n = bytes[i];
            if (n == 0)
                break;
            n = ((n >> 4) & 0x0f) | ((n & 0x0f) << 4);
            n = n ^ 0x09f;
            bytes[i] = (byte)n;
        }

        try {
            return new String(bytes, DEF_STRING_CHARSET);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        return null;
    }

    /**
     * 原理：对“逻辑机号+销售期号+流水号+销售日期时间+注码”计算校验数据，每8个字节进行异或，最终结果转化为BASE64码
     *
     * @param value
     * @return
     */
    public static String getCheckCode(String value) {
        if (value == null) {
            return null;
        }

        int paramLen = value.length();
        char paramBuff[] = value.toCharArray();

        char resultBuff[] = new char[12];
        int resultIdx = 0;

        int count = (paramLen + 7) / 8;
        char sXor[] = new char[8];

        for (int i = 0; i < count - 1; i++) {
            for (int j = 0; j < 8; j++) {
                sXor[j] ^= paramBuff[i*8+j];
            }
        }

        for (int i = (count - 1) * 8; i < paramLen; i++) {
            sXor[i- (count - 1) * 8] ^= paramBuff[i];
        }

        char BASE64_VALUES_BUFF[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/".toCharArray();

        for (int i = 0; i < 2; i++) {
            resultBuff[resultIdx++] = BASE64_VALUES_BUFF[sXor[i*3+0] >> 2];
            resultBuff[resultIdx++] = BASE64_VALUES_BUFF[((sXor[i*3+0] << 4) | (sXor[i*3+1] >> 4)) & 0x3f];
            resultBuff[resultIdx++] = BASE64_VALUES_BUFF[((sXor[i*3+1] << 2) | (sXor[i*3+2] >> 6)) & 0x3f];
            resultBuff[resultIdx++] = BASE64_VALUES_BUFF[sXor[i*3+2] & 0x3f];
        }

        resultBuff[resultIdx++] = BASE64_VALUES_BUFF[(sXor[6] & 0xfc) >> 2];
        resultBuff[resultIdx++] = BASE64_VALUES_BUFF[((sXor[6] & 0x03) << 4) | ((sXor[7] & 0xf0) >> 4)];
        resultBuff[resultIdx++] = BASE64_VALUES_BUFF[((sXor[7] & 0x0f) << 2)];
        resultBuff[resultIdx++] = '=';

        String result = String.valueOf(resultBuff, 0, resultIdx);

        return result;
    }
}