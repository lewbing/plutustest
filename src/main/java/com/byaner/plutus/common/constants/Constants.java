package com.byaner.plutus.common.constants;

/**
 * Created by bing on 2018/5/19.
 */
public class Constants {
    public static class GameNames {
        // 双色球玩法英文名: #2.1.2.5
        public static final String DOUBLE_COLOR_BALL = "B001";

        // 东方6+1玩法英文名: #2.1.2.5
        public static final String EASTERN_SIX_PLUS_ONE = "DP61";

        // 快3玩法英文名: #2.1.2.7
        public static final String QUICK_THREE = "K3";

        // 数字型3D玩法英文名: #2.1.2.4
        public static final String NUMBER_3D = "D3";

        // 十五选五玩法英文名
        public static final String FIVE_IN_FIFTEEN = "QL515";

        // 七乐彩玩法英文名
        public static final String SEVEN_LUCKY = "QL730";

        public static final String[] GAMES = {
                GameNames.DOUBLE_COLOR_BALL,
                GameNames.EASTERN_SIX_PLUS_ONE,
                GameNames.QUICK_THREE,
                GameNames.NUMBER_3D,
                GameNames.FIVE_IN_FIFTEEN,
                GameNames.SEVEN_LUCKY,
        };
    }

    public class CityCodes {
        public static final String JiangXi = "3631016";
    }

    public class DeviceType {
        // 机具
        public static final int DEVICE_TERMINAL = 1001;
        // 手机端
        public static final int DEVICE_MOBILE = 1002;
    }

    public class LotteryType {
        // 电脑票
        public static final int TYPE_COMPUTER_LOTTERY = 2001;
        // 即开票
        public static final int TYPE_TIMELY_LOTTERY = 2002;
    }

    public static final String SPLIT_LINE_SYMBOL = "\n";

    public static final String SPLIT_ELEM_SYMBOL = "\t";

    public static final String NETTY_TO_SERVICE_CHANNEL = "NettyServiceChannel";
    public static final String SERVICE_TO_NETTY_CHANNEL = "ServiceNettyChannel";
    public static final int REDIS_LENGTH = 1000;
    public static final String TICKET_BOUGHT_SUCCESS = "02";
    public static final String PRINTING = "19";
    public static final String SENT_TO_TERMINAL = "07";
    public static final String FAIL_PRINTING = "07";
    public static final int JEDIS_TIMEOUT = 20;
    public static final int PRINT_TIMEOUT_SECONDS = 120;
    public static final int READ_TIME_OUT = 50;
}
