package com.byaner.plutus.common.bean.terminal;

import lombok.Data;

/**
 * Created by bing on 2018/5/31.
 */
@Data
public class TerminalInfo {
    // 受卡机终端标示码
    private String id;
    private String machineNo;
    // 受卡机标识码: 商户
    private String type;
    // 终端密钥, 终端生成，终端唯一
    private String terminalKey;
    // Master key, 由管理系统添加，从数据库中获取
    private String masterKey;
    // Pin Key, 由SpringService生成
    private String pinKey;
    // Mac Key, 由SpringService生成
    private String macKey;
    // 现在终端系统时间
    private long timestamp;

    @Override
    public String toString() {
        return "TerminalInfo {id: " + id
                + ", type=" + type
                + ", terminalKey=" + terminalKey
                + ", masterKey=" + masterKey
                + ", pinKey=" + pinKey
                + ", macKey=" + macKey
                + ", timestamp=" + timestamp
                + "}";
    }
}
