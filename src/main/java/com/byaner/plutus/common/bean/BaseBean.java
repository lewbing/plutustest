package com.byaner.plutus.common.bean;

import lombok.Data;

@Data
public class BaseBean {
    private String terminalId;
    private int deviceType;
}
