package com.byaner.plutus.common.bean.user;

import com.byaner.plutus.common.bean.lottery.LotteryInfo;
import com.byaner.plutus.common.utils.DateUtils;
import com.byaner.plutus.common.utils.lottery.LotteryUtils;
import lombok.Data;

/**
 * Created by bing on 2018/9/3.
 */
@Data
public class MemberExchangeDetail {
    // ID号
    private int id;
    // 标识
    private String guid;
    // 会员帐号
    private String account;
    // 兑奖类型
    private String exchType;
    // 彩种代码
    private String gameName;
    // 彩种期号
    private String termCode;
    // 兑奖票号
    private String ticketCode;
    // 中心票号
    private String uniteId;
    // 安全验证码
    private String safeCode;
    // 兑奖结果: 0010: 待开奖, 0011: 未中奖, 0012: 中奖, 0013: 重复兑奖,
    //      0014: 中奖金额超限, 0015: 兑奖失败, 0016: 通讯异常, 0017: 兑奖超时, 0018: 该票尚未入站或者外省票,
    //      0019: 已兑奖
    private String result;
    // 中奖金额
    private float winMoney;
    // 兑奖时间
    private String exchTime;
    // 图片名称
    private String picName;
    // 上报时间
    private String sendTime;
    // 彩票交易流水号
    private String ticketId;
    // 备注
    private String remark;

    // order_detail表开奖时间
    private String winTime;
    // order_detail设备类型
    private String deviceType;

    public MemberExchangeDetail() {
        safeCode = "00000000";
    }

    public static MemberExchangeDetail transfer(LotteryInfo info) {
        MemberExchangeDetail memberExchange = new MemberExchangeDetail();

        memberExchange.setGuid(LotteryUtils.getUuid());
        memberExchange.setAccount(info.getMobileNo());
        memberExchange.setGameName(info.getGameName());
        memberExchange.setUniteId(info.getTicketCode());
        memberExchange.setSendTime(DateUtils.getDateTime());
        memberExchange.setTicketId(LotteryUtils.getTicketId());
        memberExchange.setResult("0010");
        memberExchange.setTermCode(info.getSellTermCode());

        return memberExchange;
    }
}
