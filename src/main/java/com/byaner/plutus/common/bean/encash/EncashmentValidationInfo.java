package com.byaner.plutus.common.bean.encash;

import lombok.Data;

/**
 * Created by bing on 2018/6/8.
 *
 * 定义兑奖验证或撤消对象, REST接口请求对象 (#1.4. 思乐销售系统兑奖时请求销售系统验证兑奖合法性)
 */
@Data
public class EncashmentValidationInfo {
    // 成员变量数
    private static final int MAX_ELEMNS = 5;

    // 玩法英文名称
    private String gameName;
    // 电话号码
    private String phoneNo;
    // 密码
    // TODO: 这是什么密码?
    private String passwd;
    // 身份证号
    private String idCard;
    // 票面密码
    private String ticketCode;

    public String to8583String() {
        String result;
        String fmt = "";

        for (int i = 0; i < MAX_ELEMNS; i++) {
            fmt += "{" + i + "}\t";
        }

        result = String.format(fmt,
                gameName,
                phoneNo,
                passwd,
                idCard,
                ticketCode);

        return result;
    }
}
