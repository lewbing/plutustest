package com.byaner.plutus.common.bean.bonus;

import com.byaner.plutus.common.constants.Constants;
import com.byaner.plutus.common.utils.lottery.BonusUtils;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by bing on 2018/6/9.
 *
 * 定义中奖数据对象 (#1.3.1. 中奖数据文件格式(FTP 获取))
 */
@Data
public class BonusData {
    private static final int MIN_ELEMS = 41;

    /**
     * 定义大奖标志
     */
    // 0: 小奖
    public static final int BONUS_TYPE_SMALL = 0;
    // 1: 大奖
    public static final int BONUS_TYPE_BIG = 1;

    /**
     * 定义兑奖标志
     */
    // 0: 未兑奖
    public static final int NOT_ENCASH = 0;
    // 1: 已兑奖
    public static final int ENCASHED = 1;

    private int id;

    // 票面票号密码, char(20)
    private String ticketCode;
    // 玩法英文名称, char(10)
    private String gameName;
    // 逻辑机号, char(8)
    private String logicCode;
    // 区县代号, char(7)
    private String cityCode;
    // 有效期号, char(7)
    private String termCode;
    // 销售流水号, char(8)
    private String sellRunCode;
    // 销售日期时间, char(19)
    private String sellDate;
    // 1~N 等奖注数, char(10) * N
    // TODO: Rename to winNumber, also need to sync to the database.
    private String betNumbers = "";
    private List<String> winNumbers = new ArrayList<>();
    // 中奖金额, char(8)
    private String winMoney;
    // 身份证号, char(20)
    private String idCard;
    // 手机号码, char(30)
    private String mobileNo;
    // 电话卡号, char(30)
    private String phoneNo;
    // 电话卡密码, char(30)
    private String phoneCardPasswd;
    // 大奖标志, char(1)
    private int bonusType;
    // 弃奖日期时间(2006-03-01 00:00:00), char(19)
    private String abandonDate;
    // 兑奖标志 (0-未兑姿姿态, 1-已经兑奖), char(1)
    private int encashFlag;
    // 兑奖期号, char(7)
    private String encashTerm;
    // 兑奖日期时间(2006-01-01 23:00:00), char(19)
    private String encashDate;

    // The symbol used in the database to split the win numbers.
    private static final String SPLIT_SYMBOL = ",";

    private static BonusData parseBonusData(String data) {
        System.out.println("BonusData::parseBonusData: data=" + data);

        String[] dataList = data.split(Constants.SPLIT_ELEM_SYMBOL);
        if (dataList == null || dataList.length < MIN_ELEMS) {
            System.out.println("BonusNotice::parse8583String: Invalid data.");

            return null;
        }

        BonusData result = new BonusData();

        int idx = 0;
        result.setTicketCode(dataList[idx++]);
        result.setGameName(dataList[idx++]);
        result.setLogicCode(dataList[idx++]);
        result.setCityCode(dataList[idx++]);
        result.setTermCode(dataList[idx++]);
        result.setSellRunCode(dataList[idx++]);
        result.setSellDate(dataList[idx++]);

        // 长周期30个注数, 短周期50个注数
        int maxBonusNumbers = BonusUtils.getMaxBonusNumber(result.getGameName()) + idx;
        for (;idx < maxBonusNumbers; idx++) {
            result.getWinNumbers().add(dataList[idx]);
            result.betNumbers += dataList[idx] + SPLIT_SYMBOL;
        }

        result.setWinMoney(dataList[idx++]);
        result.setIdCard(dataList[idx++]);
        result.setMobileNo(dataList[idx++]);
        result.setPhoneNo(dataList[idx++]);
        result.setPhoneCardPasswd(dataList[idx++]);
        result.setBonusType(Integer.parseInt(dataList[idx++]));
        result.setAbandonDate(dataList[idx++]);
        result.setEncashFlag(Integer.parseInt(dataList[idx++]));
        result.setEncashTerm(dataList[idx++]);
        result.setEncashDate(dataList[idx++]);

        System.out.println("BonusData::parseBonusData: result=" + result);

        return result;
    }

    public static List<BonusData> parse8583String(String data) {
        if (data == null || data.isEmpty()) {
            System.out.println("BonusNotice::parse8583String: Invalid argument.");

            return null;
        }

        System.out.println("BonusData::parse8583String lines=" + data.split(Constants.SPLIT_LINE_SYMBOL).length
            + ", elems=" + data.split(Constants.SPLIT_ELEM_SYMBOL).length);

        String[] dataLines = data.split(Constants.SPLIT_LINE_SYMBOL);
        if (dataLines == null || dataLines.length <= 0) {
            System.out.println("BonusNotice::parse8583String: Invalid line's data.");

            return null;
        }

        List<BonusData> result = new ArrayList<>();
        for (String line : dataLines) {
            BonusData bonus = parseBonusData(line);

            result.add(bonus);
        }

        return result;
    }

    public static final String getSplitSymbol() {
        return SPLIT_SYMBOL;
    }
}
