package com.byaner.plutus.common.bean.lottery_server;

import lombok.Data;

/**
 * Created by bing on 2018/6/25.
 */
@Data
public class DateTime {
    private static final int MIN_ELEMS = 3;

    private String date;
    private String time;

    public static DateTime parse8583String(String data) {
        if (data == null || data.isEmpty()) {
            System.out.println("DateTime::parse8583String: Invalid argument.");

            return null;
        }

        String[] dataList = data.split("\t");
        if (dataList == null || dataList.length < MIN_ELEMS) {
            System.out.println("DateTime::parse8583String: Invalid data.");
        }

        DateTime result = new DateTime();
        int idx = 1;
        result.setDate(dataList[idx++]);
        result.setTime(dataList[idx++]);

        return result;
    }

    public String toString() {
        return String.format("%s %s", date, time);
    }
}
