package com.byaner.plutus.common.bean.order.print;

/**
 * @author Andrew
 *
 */
public enum PrintStatus {
	Success("06"), Fail("07"), Partial("08"), Timeout("09"), Printing("19");
	private String code;

	public static PrintStatus toType(String code) {
		for (PrintStatus ps : PrintStatus.values()) {
			if (ps.getCode().equals(code)) {
				return ps;
			}
		}
		return null;
	}

	private PrintStatus(String code) {
		this.code = code;
	}

	public String getCode() {
		return code;
	}
}
