package com.byaner.plutus.common.bean.lottery_server;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by bing on 2018/6/9.
 *
 * 定义彩票游戏参数对象 (#1.1.1. 新期参数文件格式(FTP 获取))
 */
@Data
public class LotteryParameter {
    /**
     * 定义玩法权限
     */
    // 取消权限
    public static final int PERMISSION_WITHDRAW = 0;
    // 只售权限
    public static final int PERMISSION_SELL = 1;
    // 只兑权限
    public static final int PERMISSION_ENCASH = 2;
    // 售兑权限
    public static final int PERMISSION_SELL_ENCASH = 3;

    private static final int MIN_ELEMS = 9;

    private int id;

    // 发起购彩设备类型
    private int deviceType;

    // 玩法英文名称, char(10)
    private String gameName;
    // 期号(期号为七位), char(7)
    private String termCode;
    // 开期日期时间(2006-01-01 00:00:01), char(19)
    private String beginTermDate;
    // 封机日期时间(2006-01-01 23:59:59), char(19)
    private String endTermDate;
    // 最小机号, char(8)
    private String minStation;
    // 最大机号, char(8)
    private String maxStation;
    // 站点销售最大流水号(流水号范围定义为 00001 到 16000，定义长度 10 为主要为了以后扩充), char(10)
    private String maxRunCode;
    // 玩法权限(0:取消，1:只售，2:只兑，3:售 兑)(山东), char(2)
    private int permission;
    // 最大多期期数，最大值为 31, char(2)
    private String maxTermNum;
    // 多期期号 1~N
    private List<String> multiTermCodes = new ArrayList<String>();

    public static LotteryParameter parse8583String(String data) {
        if (data == null || data.isEmpty()) {
            System.out.println("LotteryParameter::parse8583String: Invalid argument.");

            return null;
        }

        String[] dataList = data.split("\t");
        if (dataList == null || dataList.length < MIN_ELEMS) {
            System.out.println("LotteryParameter::parse8583String: Invalid data.");
        }

        LotteryParameter result = new LotteryParameter();
        int idx = 0;
        result.setGameName(dataList[idx++]);
        result.setTermCode(dataList[idx++]);
        result.setBeginTermDate(dataList[idx++]);
        result.setEndTermDate(dataList[idx++]);
        result.setMinStation(dataList[idx++]);
        result.setMaxStation(dataList[idx++]);
        result.setMaxRunCode(dataList[idx++]);
        result.setPermission(Integer.parseInt(dataList[idx++]));
        result.setMaxTermNum(dataList[idx++]);

        for (;idx < dataList.length; idx++) {
            result.getMultiTermCodes().add(dataList[idx]);
        }

        return result;
    }
}
