package com.byaner.plutus.common.bean.selling;

import com.byaner.plutus.common.constants.Constants;
import lombok.Data;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by bing on 2018/6/9.
 *
 * 定义销售数据对象 (#1.6.1. 销售文件格式(FTP 获取，有两个文件))
 */
@Data
public class SellingData {
    private static final int MIN_ELEMS = 14;

    private int id;

    // 票号, char(23)
    private String ticketCode;
    // 玩法英文名, char(10)
    private String gameName;
    // 销售期号, char(8)
    private String sellTermCode;
    // 有效期号, char(8)
    private String validTermCode;
    // 逻辑机号, char(9)
    private String logicCode;
    // 区县代号, char(7)
    private String cityCode;
    // 流水号, char(10)
    private String runCode;
    // 玩法代号, char(2)
    // TODO: 玩法代号还有哪些?
    private String playType;
    // 销售时间(2006-01-01 23:59:59), char(19)
    private String sellDate;
    // 注数, char(5)
    private String betNum;
    // 退奖标志, char(2)
    private String withdrawFlag;
    // 打印标志, char(2)
    // TODO: 打印标志是什么意思，会有哪些选择?
    private String printFlag;
    // 注码, char(150)
    private String betCode;
    // 手机号, char(13)
    // TODO: 这里为什么会有手机号呢?
    private String mobileNo;

    private static final Logger mLogger = LoggerFactory.getLogger(SellingData.class);

    private static SellingData parseSellingData(String data) {
        mLogger.info("SellingData::parseSellingData: data=" + data);

        String[] dataList = data.split(Constants.SPLIT_ELEM_SYMBOL);
        if (dataList == null || dataList.length < MIN_ELEMS) {
            mLogger.error("SellingData::parseSellingData: Invalid data.");

            return null;
        }

        SellingData result = new SellingData();
        int idx = 0;

        result.setTicketCode(dataList[idx++]);
        result.setGameName(dataList[idx++]);
        result.setSellTermCode(dataList[idx++]);
        result.setValidTermCode(dataList[idx++]);
        result.setLogicCode(dataList[idx++]);
        result.setCityCode(dataList[idx++]);
        result.setRunCode(dataList[idx++]);
        result.setPlayType(dataList[idx++]);
        result.setSellDate(dataList[idx++]);
        result.setBetNum(dataList[idx++]);
        result.setWithdrawFlag(dataList[idx++]);
        result.setPrintFlag(dataList[idx++]);
        result.setBetCode(dataList[idx++]);
        result.setMobileNo(dataList[idx++]);

        mLogger.info("SellingData::parseSellingData: result=" + result);

        return result;
    }

    public static List<SellingData> parse8583String(String data) {
        if (data == null || data.isEmpty()) {
            mLogger.error("SellingData::parse8583String: Invalid argument.");

            return null;
        }

        mLogger.info("SellingData::parse8583String lines=" + data.split(Constants.SPLIT_LINE_SYMBOL).length
                + ", elems=" + data.split(Constants.SPLIT_ELEM_SYMBOL).length);

        String[] dataLines = data.split(Constants.SPLIT_LINE_SYMBOL);
        if (dataLines == null || dataLines.length <= 0) {
            mLogger.error("SellingData::parse8583String: Invalid line's data.");

            return null;
        }

        List<SellingData> result = new ArrayList<>();
        for (String line : dataLines) {
            SellingData selling = parseSellingData(line);
            if (selling == null) {
                mLogger.error("parse8583String: parseSellingData was failed.");
                return null;
            }

            result.add(selling);
        }

        return result;
    }
}
