package com.byaner.plutus.common.bean.order.print;

/**
 * @author Andrew
 *
 */
public enum PrintStatusExt {
	Success("00"), Fail("01"), SendingFailure("02"), TimeOut("03"), NothingPrint("05"), Sending("06"), Sent("07");
	private String code;

	private PrintStatusExt(String code) {
		this.code = code;
	}

	public String getCode() {
		return code;
	}

	public static PrintStatusExt toType(String code) {
		for (PrintStatusExt printStatusExt : PrintStatusExt.values()) {
			if (printStatusExt.getCode().equals(code)) {
				return printStatusExt;
			}
		}
		return null;
	}
}
