package com.byaner.plutus.common.bean.terminal;

/**
 * @author Andrew
 *
 */
public class SoftwareVersion {
	private String terminalId;
	private String softversion;
	private String mainVersion;
	private String adAppVersion;

	public String getTerminalId() {
		return terminalId;
	}

	public void setTerminalId(String terminalId) {
		this.terminalId = terminalId;
	}

	public String getSoftversion() {
		return softversion;
	}

	public void setSoftversion(String softversion) {
		this.softversion = softversion;
	}

	public String getMainVersion() {
		return mainVersion;
	}

	public void setMainVersion(String mainVersion) {
		this.mainVersion = mainVersion;
	}

	public String getAdAppVersion() {
		return adAppVersion;
	}

	public void setAdAppVersion(String adAppVersion) {
		this.adAppVersion = adAppVersion;
	}

}
