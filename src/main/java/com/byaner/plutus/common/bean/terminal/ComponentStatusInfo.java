package com.byaner.plutus.common.bean.terminal;

/**
 * @author Andrew
 *
 */
public class ComponentStatusInfo {
	private String terminalId;
	private String partsCode;
	private String statusCode;
	private String deal;
	private String stateTime;
	private String sendTime;
	private String remark;
	private String expand1;
	private String expand2;

	public String getTerminalId() {
		return terminalId;
	}

	public void setTerminalId(String terminalId) {
		this.terminalId = terminalId;
	}

	public String getPartsCode() {
		return partsCode;
	}

	public void setPartsCode(String partsCode) {
		this.partsCode = partsCode;
	}

	public String getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}

	public String getDeal() {
		return deal;
	}

	public void setDeal(String deal) {
		this.deal = deal;
	}

	public String getStateTime() {
		return stateTime;
	}

	public void setStateTime(String stateTime) {
		this.stateTime = stateTime;
	}

	public String getSendTime() {
		return sendTime;
	}

	public void setSendTime(String sendTime) {
		this.sendTime = sendTime;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getExpand1() {
		return expand1;
	}

	public void setExpand1(String expand1) {
		this.expand1 = expand1;
	}

	public String getExpand2() {
		return expand2;
	}

	public void setExpand2(String expand2) {
		this.expand2 = expand2;
	}

	/**
	 * 
	 */
	public ComponentStatusInfo() {
	}

	@Override
	public String toString() {
		return "ComponentStatusInfo [terminalId=" + terminalId + ", partsCode=" + partsCode + ", statusCode="
				+ statusCode + ", deal=" + deal + ", stateTime=" + stateTime + ", sendTime=" + sendTime + ", remark="
				+ remark + ", expand1=" + expand1 + ", expand2=" + expand2 + "]";
	}

}
