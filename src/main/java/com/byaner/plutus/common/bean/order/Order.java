package com.byaner.plutus.common.bean.order;

import lombok.Data;

/**
 * Created by bing on 2018/8/28.
 */
@Data
public class Order {
    public static final String STATUS_BOUGHT_SUCCEEDED = "02";
    public static final String STATUS_BOUGHT_FAILED = "03";
    public static final String STATUS_SOME_BOUGHT_SUCCEEDED = "04";

    // 订单号
    private String orderId;
    // 终端编号
    private String terminalId;
    // 彩票类型, 1: 电脑票, 2: 即开票
    private String lotteryType;
    // 会员标识Id
    private String openId;
    // 会员帐户
    private String account;
    // 微信订单号
    private String transactionId;
    // 订单总金额
    private String orderAmount;
    // 使用余额支付金额
    private double money;
    // 支付乐币数
    private int lebi;
    // 实付金额
    private double realAmount;
    // 出票成功金额
    private double succAmount;
    // 出票失败金额
    private double failAmount;
    // 优惠金额
    private double discountAmount;
    // 订单积分
    private int integral;
    // 订单状态
    private String status;
    // 订单时间
    private String orderTime;
    // 支付时间
    private String payTime;
    // 结束时间
    private String endTime;
    // 通知标识
    private String tzbs;
    // 订单类型
    private String orderType;
    // TODO: 这个字段有什么意义?
    private String idPtn;

    // 发起购彩设备类型
    int deviceType;

    public Order() {
        terminalId = "00000000";
    }
}
