package com.byaner.plutus.common.bean.selling;

import com.byaner.plutus.common.constants.Constants;
import lombok.Data;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by bing on 2018/6/9.
 *
 * 定义销售数据对象 (#1.6.1. 销售文件格式(FTP 获取，有两个文件))
 */
@Data
public class StatSellingData {
    private static final int MIN_STAT_ELEMS = 12;

    private int id;

    // 玩法英文名, char(10)
    private String gameName;
    // 期号, char(8)
    private String termCode;
    // 销售票总数, char(8)
    private String sellCount;
    // 销售总金额, char(8)
    private String totalMoney;
    // 有效票数, char(8)
    private String validTicketCount;
    // 有效票金额, char(8)
    private String totalValidTicketMoney;
    // 注销票数, char(8)
    private String withdrawTicketCount;
    // 注销票金额, char(8)
    private String withdrawTicketMoney;
    // 无效票数, char(8)
    private String invalidTicketCount;
    // 无效票金额, char(8)
    private String invalidTicketMoney;
    // 未完成票数, char(8)
    private String undoneTicketCount;
    // 未完成票金额, char(8)
    private String undoneTicketMoney;

    private static final Logger mLogger = LoggerFactory.getLogger(SellingData.class);

    public static StatSellingData parseStat8583String(String data) {
        if (data == null || data.isEmpty()) {
            mLogger.error("SellingData::parseStat8583String: Invalid argument.");

            return null;
        }

        mLogger.info("SellingData::parseStat8583String: data=" + data);

        String[] dataList = data.split(Constants.SPLIT_ELEM_SYMBOL);
        if (dataList == null || dataList.length < MIN_STAT_ELEMS) {
            mLogger.error("SellingData::parseStat8583String: Invalid data.");

            return null;
        }

        StatSellingData statData = new StatSellingData();

        int idx = 0;
        statData.setGameName(dataList[idx++]);
        statData.setTermCode(dataList[idx++]);
        statData.setSellCount(dataList[idx++]);
        statData.setTotalMoney(dataList[idx++]);
        statData.setValidTicketCount(dataList[idx++]);
        statData.setTotalValidTicketMoney(dataList[idx++]);
        statData.setWithdrawTicketCount(dataList[idx++]);
        statData.setWithdrawTicketMoney(dataList[idx++]);
        statData.setInvalidTicketCount(dataList[idx++]);
        statData.setInvalidTicketMoney(dataList[idx++]);
        statData.setUndoneTicketCount(dataList[idx++]);
        statData.setUndoneTicketMoney(dataList[idx++]);

        return statData;
    }
}
