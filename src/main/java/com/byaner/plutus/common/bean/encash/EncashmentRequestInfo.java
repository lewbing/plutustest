package com.byaner.plutus.common.bean.encash;

import lombok.Data;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by bing on 2018/6/2.
 *
 * 定义兑奖命令对象，REST接口请求对象 (#2.3. 兑奖命令格式)
 */
@Data
public class EncashmentRequestInfo {
    private Logger mLogger = LoggerFactory.getLogger(EncashmentRequestInfo.class);

    // 成员变量数
    private static final int MAX_ELEMS = 5;

    // 玩法英文名称, char(10)
    private String gameName;
    // 逻辑机号, char(8)
    private String logicCode;
    // 兑奖期号, char(7)
    private String termCode;
    // 中奖金额, char(7)
    private float winMoney;
    // 票面密码, char(20)
    private String ticketCode;

    public String to8583String() {
        String result = "";
        String fmt = "";

        for (int i = 0; i < MAX_ELEMS; i++) {
            fmt += "%s" + "\t";
        }

        result = String.format(fmt, gameName, logicCode, termCode, (int)(winMoney), ticketCode);

        mLogger.info("to8583String: fmt=" + fmt + ", result=" + result);

        return result;
    }
}
