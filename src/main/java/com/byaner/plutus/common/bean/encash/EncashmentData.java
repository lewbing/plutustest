package com.byaner.plutus.common.bean.encash;

import com.byaner.plutus.common.constants.Constants;
import lombok.Data;

/**
 * Created by bing on 2018/6/9.
 *
 * 定义兑奖文件对象 (#1.7.1. 兑奖文件格式(FTP 获取，两个文件))
 */
@Data
public class EncashmentData {
    private static final int MIN_ELEMS = 10;

    private static final int MIN_STAT_ELEMS = 4;

    /**
     * 定义幸运玩法类型
     */
    // 0: 主玩法
    public static final int LUCK_TYPE_MAINLY = 0;
    // 1: 幸运玩法
    public static final int LUCK_TYPE_LUCKY = 1;

    private int id;

    // 玩法英文名, char(10)
    private String gameName;
    // 幸运玩法类型, char(2)
    private int luckType;
    // 销售期号, char(8)
    private String sellTermCode;
    // 有效期号, char(8)
    private String validTermCode;
    // 销售逻辑机号, char(9)
    private String logicCode;
    // 流水号, char(10)
    private String runCode;
    // 总中奖金额, char(10)
    private String bonus;
    // 兑奖逻辑机号, char(9)
    private String encashLogicCode;
    // 兑奖期号, char(8)
    private String encashTermCode;
    // 兑奖时间, char(19)
    private String encashDate;

    // 期号, char(10)
    private String termCode;
    // 兑奖金额, char(8)
    private String encashSum;
    // 兑奖个数, char(8)
    private String encashCount;

    public static EncashmentData parse8583String(String data) {
        if (data == null || data.isEmpty()) {
            System.out.println("EncashmentData::parse8583String: Invalid argument.");

            return null;
        }

        System.out.println("EncashmentData::parse8583String: data=" + data);

        String[] dataList = data.split(Constants.SPLIT_ELEM_SYMBOL);
        if (dataList == null || dataList.length < MIN_ELEMS) {
            System.out.println("EncashmentData::parse8583String: Invalid data.");

            return null;
        }

        EncashmentData result = new EncashmentData();
        int idx = 0;
        result.setGameName(dataList[idx++]);
        result.setLuckType(Integer.parseInt(dataList[idx++]));
        result.setSellTermCode(dataList[idx++]);
        result.setValidTermCode(dataList[idx++]);
        result.setLogicCode(dataList[idx++]);
        result.setRunCode(dataList[idx++]);
        result.setBonus(dataList[idx++]);
        result.setEncashLogicCode(dataList[idx++]);
        result.setEncashTermCode(dataList[idx++]);
        result.setEncashDate(dataList[idx++]);

        return result;
    }

    public EncashmentData parseStat8583String(String data) {
        if (data == null || data.isEmpty()) {
            System.out.println("EncashmentData::parseStat8583String: Invalid argument.");

            return null;
        }

        System.out.println("EncashmentData::parseStat8583String: data=" + data);

        String[] dataList = data.split(Constants.SPLIT_ELEM_SYMBOL);
        if (dataList == null || dataList.length < MIN_STAT_ELEMS) {
            System.out.println("EncashmentData::parseStat8583String: Invalid data.");

            return null;
        }

        int idx = 0;
        this.setGameName(dataList[idx++]);
        this.setTermCode(dataList[idx++]);
        this.setEncashSum(dataList[idx++]);
        this.setEncashCount(dataList[idx++]);

        return this;
    }
}
