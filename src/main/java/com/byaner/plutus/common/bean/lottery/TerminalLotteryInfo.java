package com.byaner.plutus.common.bean.lottery;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class TerminalLotteryInfo {
    // 注码, char(120), *
    private List<String> code = new ArrayList();
    // 注数, char(8), *
    private String betType;
}
