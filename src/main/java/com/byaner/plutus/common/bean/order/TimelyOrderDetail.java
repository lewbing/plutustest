package com.byaner.plutus.common.bean.order;

import com.byaner.plutus.common.bean.lottery.LotteryInfo;
import lombok.Data;

@Data
public class TimelyOrderDetail {
    // 终端编号
    private String terminalId;
    // 订单编号
    private String orderId;
    // 票面Id
    private String ticketId;
    // 交易时间
    private String saleTime;
    // 设备交易流水号
    private String tranNo;
    // 出票通道编号
    private String channelNo;
    // 记录类型
    private String recordType;
    // 彩种代码
    private String gameCode;
    // 彩种名称
    private String gameName;
    // 彩票单价
    private float money;
    // 剩余数量
    private int remain;
    // 操作数量
    private int operNum;
    // 上报时间
    private String sendTime;

    public static TimelyOrderDetail transfer(String orderId, String ticketId, String terminalId, LotteryInfo info) {
        TimelyOrderDetail order = new TimelyOrderDetail();

        order.setOrderId(orderId);
        order.setTicketId(ticketId);
        order.setTerminalId(terminalId);
        order.setSaleTime(info.getSaleTime());
        order.setTranNo(info.getTranNo());
        order.setChannelNo(info.getChannelNo());
        order.setRecordType(info.getRecordType());
        order.setGameCode(info.getGameName());
        order.setMoney(info.getMoney());
        order.setRemain(info.getRemain());
        order.setOperNum(info.getOperNum());
        order.setSendTime(info.getSendTime());
        order.gameCode = info.getTicketId();
        order.gameName = info.getZhGameName();

        return order;
    }
}
