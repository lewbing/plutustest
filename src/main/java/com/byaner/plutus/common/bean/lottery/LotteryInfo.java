package com.byaner.plutus.common.bean.lottery;

import com.byaner.plutus.common.bean.order.OrderDetail;
import com.byaner.plutus.common.constants.Constants;
import com.byaner.plutus.common.modules.protocol.welfarelott.jiangxi.proto8583.utils.Encryption;
import com.byaner.plutus.common.utils.DateUtils;
import lombok.Data;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by bing on 2018/6/2.
 */

/**
 * 彩票对象数据结构 (#2.1.1 销售命令格式) (#2.2. 注销命令格式)
 */
@Data
public class LotteryInfo {
    // 后台接口返回的8583最少数据个数
    private static final int MIN_ELEMS = 2;

    // 成员变量数
    private static final int MAX_ELEMS = 18;

    public static final int FLAG_SELLING = 0;
    public static final int FLAG_WITHDRAW = 1;

    // 发起购彩设备类型
    private int deviceType;

    // TODO: Need change into the `code`.
    // 玩法英文名称, char(10), *
    private String gameName;
    // 逻辑机号, char(8),
    private String logicCode;
    // 区县代号, char(7),
    private String cityCode;
    // 销售期号, char(7), *
    private String sellTermCode;
    // 有效期号, char(7), *
    private String validTermCode;
    // 流水号, char(8), *
    private String runCode;
    // 销售日期时间, char(19), *
    private String sellDateTime;
    // 注数, char(8), *
    private String betNum;
    // 销售方式, char(1), *
    private String sellWay;
    // 投注方式, char(1), *
    private String drawWay;
    // 注码, char(120), *
    private String code;
    // 注销标识, char(1), *
    private int withdrawFlag;
    // 身份证号, char(20)
    private String idCard;
    // 手机卡号, char(30)
    private String mobileNo;
    // 电话卡号, char(30)
    private String phoneNo;
    // 电话卡密码, char(30)
    private String phoneCardPasswd;
    // 彩民记名销售卡号, char(12)
    private String sellPersonId;
    // 校验码, char(32)
    private String checkCode;

    // 票面密码, 成功请求购彩接口后, 后台返回的数据。
    private String ticketCode;

    // 本单花费金额, *
    private float money;
    // 本单花费多少币, *
    private int lebi;
    // 开奖时间, *
    private String drawTime;
    // 倍数, *
    private String mulCount;
    // 彩票玩法类型: 单式、复式、胆拖
    private String betType;
    // 交易流水号
    private String ticketId;

    // 销售时间
    private String saleTime;
    // 设备交易流水号
    private String tranNo;
    // 出票通道编号
    private String channelNo;
    // 记录类型
    private String recordType;
    // 剩余数量
    private int remain;
    // 操作数量
    private int operNum;
    // 销售序号
    private String saleOrder;
    // 上报时间
    private String sendTime;
    // 通道状态
    private String channelStatus;
    // 彩票名称
    private String zhGameName;

    public LotteryInfo() {
        // 设置默认值
        deviceType = Constants.DeviceType.DEVICE_TERMINAL;
        sellWay = "0";
        cityCode = Constants.CityCodes.JiangXi;
        betNum = "1";
        drawWay = "0";
        withdrawFlag = FLAG_SELLING;
        mobileNo = "0";
        phoneCardPasswd = "0";
        sellDateTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
        sellPersonId = "0";
        checkCode = "";
    }

    /**
     * The check code will be generate inside and don't allow others to modify it.
     *
     * @param checkCode
     */
    private void setCheckCode(String checkCode) {
        this.checkCode = checkCode;
    }

    public String to8583String() {
        String result;
        String fmt = "";

        for (int i = 0; i < MAX_ELEMS; i++) {
            fmt += "%s" + "\t";
        }

        /**
         * 对“逻辑机号+销售期号+流水号+销售日期时间+注码”计算校验数据
         */
        if (checkCode == "") {
            String checkCodeData = logicCode + sellTermCode + runCode + sellDateTime + code;
            checkCode = Encryption.getCheckCode(checkCodeData);
        }

        result = String.format(fmt,
                gameName,
                logicCode,
                cityCode,
                sellTermCode,
                validTermCode,
                runCode,
                sellDateTime,
                betNum,
                sellWay,
                drawWay,
                code,
                withdrawFlag,
                idCard,
                mobileNo,
                phoneNo,
                phoneCardPasswd,
                sellPersonId,
                checkCode);

        return result;
    }

    public LotteryInfo parse8583String(String data) {
        if (data == null) {
            System.out.println("LotteryInfo::parse8583String: Invalid argument.");

            return null;
        }

        String[] dataList = data.split("\t");
        if (dataList == null || dataList.length < MIN_ELEMS) {
            System.out.println("LotteryInfo::parse8583String: Invalid data.");
        }

        int idx = 1;
        this.setTicketCode(dataList[idx++]);

        return this;
    }

    public static LotteryInfo transfer(OrderDetail orderDetail) {
        LotteryInfo info = new LotteryInfo();

        info.setCityCode(Constants.CityCodes.JiangXi);
        info.setGameName(orderDetail.getGameName());

        info.setSellTermCode(orderDetail.getTermCode());
        info.setValidTermCode(orderDetail.getTermCode());

        String betNumValue = String.valueOf(orderDetail.getBetCount());
        try {
            betNumValue = String.valueOf(orderDetail.getBetCount() * orderDetail.getMulCount());
        } catch (Exception e) {
            e.printStackTrace();
        }

        info.setBetNum(betNumValue);
        info.setMulCount(String.valueOf(orderDetail.getMulCount()));
        info.setCode(orderDetail.getCodes());
        info.setDrawWay("0");
        info.setSellWay("0");
        info.setSellDateTime(DateUtils.getDateTime());

        return info;
    }
}
