package com.byaner.plutus.common.bean.user;

import lombok.Data;

/**
 * Created by bing on 2018/8/28.
 */
@Data
public class Member {
    // 会员Id
    private int id;
    // 会员标识Id(仅用于公众号)
    private String openId;
    // 会员唯一标识
    private String uuid;
    // 会员帐户名
    private String userNo;
    // 会员昵称
    private String nickName;
    // 登录密码
    private String passwd;
    // 手机号码
    private String mobileNo;
    // 电子邮件
    private String email;
    // 帐户资金
    private float money;
    // 乐币
    private int lebi;
    // 会员类型
    private String type;
    // 会员等级
    private String level;
    // 会员积分
    private int score;
    // 会员来源
    private String fromType;
    // 注册时间
    private String regTime;
    // 状态
    private String status;
    // 终端编号
    private String terminalId;
}
