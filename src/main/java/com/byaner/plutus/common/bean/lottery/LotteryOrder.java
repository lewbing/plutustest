package com.byaner.plutus.common.bean.lottery;

import com.byaner.plutus.common.constants.Constants;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by bing on 2018/9/3.
 */
@Data
public class LotteryOrder {
    // 订单Id
    String orderId;
    // 用户帐户 / 手机号
    String mobileNo;
    // 自助终端Id
    String terminalId;
    // 发起购彩设备类型 refers from @DeviceType
    int deviceType;
    // 彩种类型 refers from @LotteryType
    int lotteryType;
    // 订单金额
    float orderMoney;
    // 订单乐币
    int lebi;
    // 支付方式
    int payType;
    // 每一注彩票信息
    List<LotteryInfo> lotteries = new ArrayList<LotteryInfo>();

    public LotteryOrder() {
        // 设置默认值
        deviceType = Constants.DeviceType.DEVICE_TERMINAL;
        lotteryType = Constants.LotteryType.TYPE_COMPUTER_LOTTERY;
        terminalId = "00000000";
    }
}
