package com.byaner.plutus.common.bean.terminal;

import lombok.Data;

@Data
public class TerminalParameter {
	// 终端Id
	private String terminalId;
	// 终端参数指令类型
	private String code;
	// 终端参数命令
	private String command;
}
