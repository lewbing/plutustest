package com.byaner.plutus.common.bean.order;

import lombok.Data;

/**
 * Created by bing on 2018/6/15.
 */
@Data
public class OrderDetail {
	// 购票成功状态
    public static final String STATUS_CREATED_ORDER = "00";
    public static final String STATUS_BUYING = "01";
    public static final String STATUS_BUY_SUCCEEDED = "02";
    public static final String STATUS_BUY_FAILED = "03";
    public static final String STATUS_PAY_SUCCEEDED = "20";

    private String orderId;
	// 打票终端编号
	private String terminalId;
	// 订单交易流水号
	private String ticketId;
    // 注单号
    private String itemId;
    // 游戏玩法代号 (lottery_id, 彩种代码)
    private String gameName;
    // 注码
    private String codes;
	// 期号
	private String termCode;
	// 游戏玩法名 (salegame, 游戏玩法名)
	private String zhGameName;
    // 投注类型
    private String betType;
	// 注数
	private int betCount;
	// 倍数
	private int mulCount;
	// 金额
	private float money;
    /*
     * // 出票状态 00: 订单生成 01: 出票中 02: 出票成功 (即开票初始状态为02) 03: 出票失败 05: 福彩返回超时 06: 打票成功 07: 打票失败 09: 终端返回超时
     */
    private String status;
	// 附加码，默认为8位整数随机数
	private String additionalCode;
	// 销售时间
	private String addTime;
    // 中心票号 / 票面密码(ticketCode)
	private String uniteId;
    // 出票时间
	private String outTicketTime;
    // 打票时间
	private String printTime;
	// 本期开奖时间
	private String drawTime;
	// 本期最后兑奖结束时间
	private String validTime;
	// 流水号
	private String runCode;
	// 备注
	private String remark;
	// Res1
	private String res1;
	// Res2
	private String res2;

	// 发起购彩设备类型
	int deviceType;

	public OrderDetail() {
		terminalId = "00000000";
        status = STATUS_CREATED_ORDER;
	}
}
