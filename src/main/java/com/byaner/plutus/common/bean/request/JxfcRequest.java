package com.byaner.plutus.common.bean.request;

import com.byaner.plutus.common.bean.lottery.LotteryInfo;
import com.byaner.plutus.common.utils.DateUtils;
import lombok.Data;

@Data
public class JxfcRequest {
    private String terminalId;
    private String memberId;
    private String orderId;
    private String ticketId;
    private int deviceType;
    private String command;
    private String requestParam;
    private String requestTimestamp;
    private String response;
    private String responseCode;
    private String responseMessage;
    private String responseTimestamp;

    public JxfcRequest() {
        requestTimestamp = DateUtils.getDateTime();
    }

    public static JxfcRequest transfer(LotteryInfo lottery) {
        if (lottery == null) {
            return null;
        }

        JxfcRequest request = new JxfcRequest();

        request.setDeviceType(lottery.getDeviceType());
        request.setMemberId(lottery.getMobileNo());
        request.setTicketId(lottery.getTicketId());

        return request;
    }
}
