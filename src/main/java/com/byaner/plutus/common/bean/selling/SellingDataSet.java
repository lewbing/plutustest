package com.byaner.plutus.common.bean.selling;

import lombok.Data;

import java.util.List;

/**
 * Created by bing on 2018/8/25.
 */
@Data
public class SellingDataSet {
    private List<SellingData> sellingData;

    private StatSellingData statSellingData;
}
