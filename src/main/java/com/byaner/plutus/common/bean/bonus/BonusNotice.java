package com.byaner.plutus.common.bean.bonus;

import java.util.ArrayList;
import java.util.List;

import com.byaner.plutus.common.constants.Constants;
import com.byaner.plutus.common.utils.lottery.BonusUtils;

import lombok.Data;

/**
 * Created by bing on 2018/6/9.
 *
 * 定义开奖公告文件对象 (#1.2.1. 开奖公告文件格式(FTP 获取))
 */
@Data
public class BonusNotice {
    private static final int MIN_ELEMS = 41;

    private int id;

    // 玩法英文名称, char(10)
    private String gameName;
    // 期号, char(7)
    private String termCode;
    // 中奖号码, char(20)
    private String winBaseCode;
    // 特别号码, char(10)
    private String winSpecialCode;
    // 本期实际销售额, char(10)
    private String sellAmount;
    // 本期有效销售额, char(10)
    private String validSellAmount;
    // 1~N 等奖注数, char(10) * N (短周期玩法50个，长周期玩法30个奖)
    private String winNumber = "";
    private List<String> winNumbers = new ArrayList<>();
    // 1~N 等奖中奖金额 (短周期玩法50个，长周期玩法30个奖)
    private String winMoney = "";
    private List<String> winMonies = new ArrayList<>();
    // 滚入下期
    private String nextPeriod;
    // 幸运彩1~N实际销售额
    private String luckTotalMoney = "";
    private List<String> luckTotalMonies = new ArrayList<>();
    // 幸运彩1~N有效销售额
    private String luckValidMoney = "";
    private List<String> luckValidMonies = new ArrayList<>();
    // 幸运彩1~N奖金
    private String luckWinMoney = "";
    private List<String> luckWinMonies = new ArrayList<>();
    // 幸运彩1~N注数
    private String luckWinNumber = "";
    private List<String> luckWinNumbers = new ArrayList<>();

    // The symbol used in the database to split the win numbers and win money.
    private static final String SPLIT_SYMBOL = ",";

    public static BonusNotice parse8583String(String data) {
        if (data == null || data.isEmpty()) {
            System.out.println("BonusNotice::parse8583String: Invalid argument.");

            return null;
        }

        String[] dataList = data.split(Constants.SPLIT_ELEM_SYMBOL);
        if (dataList == null || dataList.length < MIN_ELEMS) {
            System.out.println("BonusNotice::parse8583String: Invalid data.");

            return null;
        }

        BonusNotice result = new BonusNotice();
        int idx = 0;
        result.setGameName(dataList[idx++]);
        result.setTermCode(dataList[idx++]);
        result.setWinBaseCode(dataList[idx++]);
        result.setWinSpecialCode(dataList[idx++]);
        result.setSellAmount(dataList[idx++]);
        result.setValidSellAmount(dataList[idx++]);

        int maxBonusNumbers = BonusUtils.getMaxBonusNumber(result.getGameName()) + idx;
        for (;idx <= maxBonusNumbers; idx+=2) {
            result.getWinMonies().add(dataList[idx]);
            result.winMoney += dataList[idx] + ",";

            result.getWinNumbers().add(dataList[idx+1]);
            result.winNumber += dataList[idx+1] + ",";
        }

        result.setNextPeriod(dataList[idx++]);

        System.out.println("BonusNotice::parse8583String: length=" + dataList.length
            + ", idx=" + idx);

        for (; idx < dataList.length; idx+=4) {
            result.luckTotalMonies.add(dataList[idx]);
            result.luckTotalMoney += dataList[idx] + SPLIT_SYMBOL;

            result.luckValidMonies.add(dataList[idx+1]);
            result.luckValidMoney += dataList[idx+1] + SPLIT_SYMBOL;

            result.luckWinMonies.add(dataList[idx+2]);
            result.luckWinMoney += dataList[idx+2] + SPLIT_SYMBOL;

            result.luckWinNumbers.add(dataList[idx+3]);
            result.luckWinNumber += dataList[idx+3] + SPLIT_SYMBOL;
        }

        return result;
    }

    public static final String getSplitSymbol() {
        return SPLIT_SYMBOL;
    }
}
