package com.byaner.plutus.common.exception;

/**
 * @author Andrew
 *
 */
public class UnknownDataTypeException extends Exception {

	/**
	 * @param message
	 */
	public UnknownDataTypeException(String name, String value) {
		super("Unknown data type for " + name + "(" + value + ")");
	}
	/**
	 * @param message
	 */
	public UnknownDataTypeException(Class c, String value) {
		super("Unknown data type for " + c.getName() + "(" + value + ")");
	}

}
